package psql

import (
	"database/sql"
	"net/http"
	"goapi/internal/domain"
	"goapi/internal/repository/access"
	"goapi/utils"
	"strings"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
	"github.com/lib/pq"
)

{{ $index := 0 }}{{ $comma := 0 }}{{ $isSoft := false }}
type {{ .Model }}DAO struct {
    gopsql.Queryer
}

func New{{ .Model }}(db gopsql.Queryer) *{{ .Model }}DAO {
	return &{{ .Model }}DAO{Queryer: db}
}

func ({{ .Name }}DAO *{{ .Model }}DAO) Table() string {
    return "{{ .Table }}"
}

func ({{ .Name }}DAO *{{ .Model }}DAO) Columns() []string {
    return []string{ {{ range $key, $column := .Columns }}{{ if $key }}, {{ end }}"{{ $column.Name }}"{{ end }} }
}

func ({{ .Name }}DAO *{{ .Model }}DAO) Values(d *domain.{{ .Model }}) (values []interface{}) {
    return append(values, {{ range $key, $column := .Columns }}{{ if $key }}, {{ end }}{{ if $column.IsArray }}pq.Array({{ end }}&d.{{ $column.ModelName }}{{ if $column.IsArray  }}){{ end }}{{ end }})
}

func ({{ .Name }}DAO *{{ .Model }}DAO) ByID(d *domain.{{ .Model }}, tx access.Transacter) (*domain.{{ .Model }},
	goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

    queryBuilder.Select(strings.Join({{ .Name }}DAO.Columns(), ","))

    queryBuilder.Add("FROM " + {{ .Name }}DAO.Table())

    queryBuilder.Add("WHERE id = ?", {{ range $key, $column := .PrimaryColumns }} {{ if $key }}, {{ end }} d.{{ $column.ModelName }}{{ end }})

	var queryer gopsql.Queryer
	queryer = {{ .Name }}DAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	{{ .Name }}Entity, e := {{ .Name }}DAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	if {{ .Name }}Entity == nil {
		return nil, e
	}

	return {{ .Name }}Entity, nil
}

func ({{ .Name }}DAO *{{ .Model }}DAO) Delete(d *domain.{{ .Model }}, tx access.Transacter) goerr.IError {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Add("DELETE FROM " + {{ .Name }}DAO.Table())

    queryBuilder.Add("WHERE id = ?", {{ range $key, $column := .PrimaryColumns }} {{ if $key }}, {{ end }} d.{{ $column.ModelName }}{{ end }})

	var queryer gopsql.Queryer
	queryer = {{ .Name }}DAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	if _, e := queryer.Exec(queryBuilder.RawSQL(), queryBuilder.Values()...); e != nil {
		return e
	}

	return nil
}

func ({{ .Name }}DAO *{{ .Model }}DAO) Save (d *domain.{{ .Model }}, tx access.Transacter)(
	*domain.{{ .Model }}, goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	if d.ID != 0 {
    	queryBuilder.Add("UPDATE " + {{ .Name }}DAO.Table())
    	queryBuilder.Add("SET {{ range $key, $column := .NonPrimaryColumns }} {{ if $key }}, {{ end }}{{ $column.Name }} = ?{{ end }}", {{ range $key, $column := .NonPrimaryColumns }} {{ if $key }}, {{ end }} {{ if $column.IsArray }}pq.Array({{ end }}&d.{{ $column.ModelName }}{{ if $column.IsArray  }}){{ end }}{{ end }})
   		queryBuilder.Add("WHERE id = ?", {{ range $key, $column := .PrimaryColumns }} {{ if $key }}, {{ end }} d.{{ $column.ModelName }}{{ end }})
   	} else {
    	queryBuilder.Add("INSERT INTO " + {{ .Name }}DAO.Table())
    	queryBuilder.Add("({{ range $key, $column := .NonPrimaryColumns }} {{ if $key }}, {{ end }}{{ $column.Name }}{{ end }}) VALUES ({{ range $key, $column := .NonPrimaryColumns }} {{ if $key }}, {{ end }}?{{ end }})", {{ range $key, $column := .NonPrimaryColumns }} {{ if $key }}, {{ end }} {{ if $column.IsArray }}pq.Array({{ end }}&d.{{ $column.ModelName }}{{ if $column.IsArray  }}){{ end }}{{ end }})
   	}

   	queryBuilder.Add("RETURNING " + strings.Join({{ .Name }}DAO.Columns(), ","))

	var queryer gopsql.Queryer
	queryer = {{ .Name }}DAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	{{ .Name }}Entity, e := {{ .Name }}DAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	return {{ .Name }}Entity, nil
}

func ({{ .Name }}DAO *{{ .Model }}DAO) Search({{ .Name }}SearchForm *domain.{{ .Model }}SearchForm, tx access.Transacter) (
	{{ .Name }}s []*domain.{{ .Model }}, pagination godict.Pagination, e goerr.IError) {
	queryBuilder := {{ .Name }}DAO.prepareSearchBuilder({{ .Name }}SearchForm)

	countErrChan := make(chan utils.CounterErr, 1)

	queryer := {{ .Name }}DAO.Queryer

    if tx != nil {
    	queryer = tx.Querier()
    }

    _, isTransaction := queryer.(*gopsql.Tx)

    if !isTransaction {
    	utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
    } else {
    	go utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
    }

	{{ .Name }}s, e = {{ .Name }}DAO.Collection(queryer.Query(queryBuilder.RawSQL(),
		queryBuilder.Values()...))
	if e != nil {
		return {{ .Name }}s, pagination, e
	}

	ch := <-countErrChan
	if ch.Err != nil {
		return {{ .Name }}s, pagination, ch.Err
	}

	pagination.Total = ch.Total

	return {{ .Name }}s, pagination, nil
}

func ({{ .Name }}DAO *{{ .Model }}DAO)  prepareSearchBuilder(
	{{ .Name }}SearchForm *domain.{{ .Model }}SearchForm) *gopsql.Builder {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join({{ .Name }}DAO.Columns(), ","))
	queryBuilder.Add(" FROM " + {{ .Name }}DAO.Table() + " WHERE 1 = 1")

	if {{ .Name }}SearchForm.Query != "" {
		queryBuilder.Add("AND name LIKE ?",
			utils.ToLike(&{{ .Name }}SearchForm.Query))
	}

	if len({{ .Name }}SearchForm.IDs) > 0 {
		queryBuilder.Add("AND id = ANY(?)",
			pq.Array(&{{ .Name }}SearchForm.IDs))
	}

	if len({{ .Name }}SearchForm.ExcludedIDs) > 0 {
		queryBuilder.Add("AND NOT (id = ANY(?))",
			pq.Array(&{{ .Name }}SearchForm.ExcludedIDs))
	}

	queryBuilder.Pagination({{ .Name }}SearchForm.Limit, {{ .Name }}SearchForm.Page-1)

	return queryBuilder
}


func ({{ .Name }}DAO *{{ .Model }}DAO) Collection(rows *sql.Rows, err goerr.IError) (collection []*domain.{{ .Model }},
    e goerr.IError) {
    collection = make([]*domain.{{ .Model }}, 0)

	if rows != nil {
    	defer rows.Close()
    }

	if err != nil {
		e = err

		return
	}

	for rows.Next() {
		{{ .Name }} := domain.{{ .Model }}{}

		err := rows.Scan({{ .Name }}DAO.Values(&{{ .Name }})...)
		if err != nil {
			e = goerr.New(err.Error())

			return
		}

		collection = append(collection, &{{ .Name }})
	}

	return collection, nil
}


func ({{ .Name }}DAO *{{ .Model }}DAO) One(rows *sql.Rows, err goerr.IError) ({{ .Name }} *domain.{{ .Model }}, e goerr.IError) {
	{{ .Name }}s, e := {{ .Name }}DAO.Collection(rows ,err)
	if e != nil {
		return
	}

	if len({{ .Name }}s) == 0 {
		return
	}

	return {{ .Name }}s[0], nil
}

type {{ .Model }}AI interface {
	Save({{ .Name }} *domain.{{ .Model }}, tx Transacter) (*domain.{{ .Model }}, goerr.IError)
	Delete({{ .Name }} *domain.{{ .Model }}, tx Transacter) goerr.IError
	ByID({{ .Name }} *domain.{{ .Model }}, tx Transacter) (*domain.{{ .Model }}, goerr.IError)
	Search({{ .Name }}SearchForm *domain.{{ .Model }}SearchForm,tx Transacter) ([]*domain.{{ .Model }}, godict.Pagination, goerr.IError)
}
