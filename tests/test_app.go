package tests

import (
	"fmt"
	"goapi/api/router"
	"goapi/internal/cache"
	"goapi/internal/core"
	"goapi/internal/domain"
	"goapi/internal/repository"
	"goapi/tools/config"
	"goapi/tools/dictionary"
	"goapi/tools/i18n"
	"goapi/tools/logger"
	"goapi/tools/mailgun"
	"goapi/utils"
	"os"

	"github.com/cadyrov/godict"
)

func NewTestApp(testRepositoryStub repository.Repository) *router.App {
	env := os.Getenv("ENV")

	configPath := fmt.Sprintf("%s/local%s.yml", utils.RootPath()+"/resources/config", env)

	cnf, e := config.New(configPath)
	if e != nil {
		panic(e)
	}

	logger := logger.New(os.Stdout)

	testApp := router.New(logger, cnf)

	translator, e := i18n.New(godict.ENLocale, cnf, logger)
	if e != nil {
		panic(e)
	}

	dict, e := dictionary.New(godict.ENLocale, cnf, logger)
	if e != nil {
		panic(e)
	}

	che, e := cache.New(logger)
	if e != nil {
		panic(e)
	}

	malgn := mailgun.NewMailGunService(cnf.Mailgun)

	cr := core.New(cnf, &testRepositoryStub, logger, translator, dict, nil, che, malgn)

	cr.InitCache()

	testApp.Core = cr

	return testApp
}

func TestContext() core.Context {
	return core.Context{Locale: godict.ENLocale, User: &domain.UserRender{
		User: domain.User{},
	}}
}
