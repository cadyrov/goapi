package entities

import "goapi/internal/domain"

func TestGroups() domain.Groups {
	return domain.Groups{
		ID:         1,
		GroupsForm: domain.GroupsForm{Name: "name"},
	}
}

func SecondTestGroups(id int64) domain.Groups {
	return domain.Groups{
		ID:         id,
		GroupsForm: domain.GroupsForm{Name: "name"},
	}
}
