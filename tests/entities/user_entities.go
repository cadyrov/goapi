package entities

import "goapi/internal/domain"

func TestUser() domain.User {
	return domain.User{
		ID:           1,
		PasswordHash: nil,
		Roles:        []string{},
		StatusID:     domain.UserStatusNew,
		UserForm: domain.UserForm{
			Username: "adsadas",
			Email:    "asd@gmail.com",
		},
	}
}

func SecondTestUser(id int64) domain.User {
	return domain.User{
		ID:           id,
		PasswordHash: nil,
		Roles:        []string{},
		StatusID:     domain.UserStatusNew,
		UserForm: domain.UserForm{
			Username: "adsadas",
			Email:    "asd@gmail.com",
		},
	}
}
