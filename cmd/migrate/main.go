package main

import (
	"fmt"
	"goapi/internal/repository"
	"goapi/tools/config"
	"goapi/tools/logger"
	"goapi/utils"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
)

func main() {
	var (
		create = "create"
		up     = "up"
		down   = "down"
	)

	env := os.Getenv("ENV")
	rootpath := utils.RootPath()
	configPath := fmt.Sprintf("%s/%s.yml", rootpath+"/resources/config", env)
	logger := logger.New(os.Stdin)

	cnf, e := config.New(configPath)
	if e != nil {
		panic(e)
	}

	repo, e := repository.New(cnf.Repository, logger, false)
	if e != nil {
		panic(e)
	}

	args := os.Args[1:]

	appType := ""
	second := "1"

	fmt.Println(appType, second)

	if len(args) > 0 {
		appType = args[0]
	}

	if len(args) > 1 {
		second = args[1]
	}

	if appType == create {
		if e := Create(*cnf, second); e != nil {
			logger.Err(e)
		}
	}

	if appType == down {
		i, err := strconv.Atoi(second)
		if err != nil {
			logger.Err(err)
		}

		if e := MigrateDown(*cnf, *repo, i); e != nil {
			logger.Err(e)
		}
	}

	if appType == up {
		if e := MigrateUp(*cnf, *repo); e != nil {
			logger.Err(e)
		}
	}
}

type MgStruct struct {
	file os.FileInfo
	mg   gopsql.Migration
	toDo bool
}

func Create(cnf config.Config, name string) (e goerr.IError) {
	if name == "" {
		e = goerr.New("please send name of new migration").HTTP(http.StatusBadRequest)

		return
	}

	migration := gopsql.Migration{
		UpSQL:   []string{"put your up sql code here; and here"},
		DownSQL: []string{"put your down sql code here"},
	}

	b, e := godict.YamlMarshal(&migration)
	if e != nil {
		return
	}

	fName := fmt.Sprintf("%d_%s.yml", time.Now().UnixNano()/int64(time.Second), name)

	err := ioutil.WriteFile(utils.RootPath()+"/"+cnf.Project.MigratePath+"/"+fName, b, 0o600)
	if err != nil {
		e = goerr.New(err.Error())
	}

	log.Println("success create new migration " + fName)

	return e
}

func MigrateUp(cnf config.Config, repo repository.Repository) (e goerr.IError) {
	tx, e := repo.PSQLQueryer.(*gopsql.DB).Begin()
	if e != nil {
		return
	}

	e = gopsql.CreateMigrationTable(tx)
	if e != nil {
		return
	}

	listMigrations, e := migrationsFrom(utils.RootPath() + "/" + cnf.Project.MigratePath)
	if e != nil {
		return
	}

	_, keys, e := createdMigration(tx)
	if e != nil {
		return
	}

	for i := range listMigrations {
		if _, ok := keys[strings.ReplaceAll(listMigrations[i].file.Name(), ".yml", "")]; ok {
			listMigrations[i].toDo = false
		}
	}

	for i := range listMigrations {
		if listMigrations[i].toDo {
			mgName := strings.ReplaceAll(listMigrations[i].file.Name(), ".yml", "")

			log.Println("start migration up" + mgName)

			e = fileToMigration(utils.RootPath()+"/"+cnf.Project.MigratePath+"/"+listMigrations[i].file.Name(),
				&listMigrations[i].mg)
			if e != nil {
				return
			}

			listMigrations[i].mg.Name = mgName

			e = listMigrations[i].mg.Up(tx)
			if e != nil {
				return
			}

			log.Println("success migration up " + mgName)
		}
	}

	errs := tx.Commit()
	if errs != nil {
		e = goerr.New(errs.Error())
	}

	return e
}

func fileToMigration(path string, mg *gopsql.Migration) (e goerr.IError) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	e = godict.YamlUnmarshal(b, mg)
	if e != nil {
		return
	}

	return e
}

func createdMigration(tx gopsql.Queryer) (list []string, keys map[string]struct{}, e goerr.IError) {
	sql := "SELECT version FROM migration ORDER BY apply_time DESC"

	keys = make(map[string]struct{})

	rows, err := tx.Query(sql)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	defer rows.Close()

	for rows.Next() {
		var name string

		err := rows.Scan(&name)
		if err != nil {
			e = goerr.New(err.Error())

			return
		}

		keys[name] = struct{}{}

		list = append(list, name)
	}

	return list, keys, nil
}

func migrationsFrom(path string) (list []MgStruct, e goerr.IError) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	if len(files) == 0 {
		return
	}

	list = make([]MgStruct, 0, len(files))

	for i := range files {
		str := MgStruct{
			file: files[i],
			mg:   gopsql.Migration{},
			toDo: true,
		}
		list = append(list, str)
	}

	return list, nil
}

func MigrateDown(cnf config.Config, repo repository.Repository, count int) (e goerr.IError) {
	tx, e := repo.PSQLQueryer.(*gopsql.DB).Begin()
	if e != nil {
		return
	}

	cm, _, e := createdMigration(tx)
	if e != nil {
		return
	}

	listMigrations := make([]MgStruct, 0, len(cm))

	for i := range cm {
		str := MgStruct{
			mg: gopsql.Migration{
				Name: cm[i],
			},
		}

		listMigrations = append(listMigrations, str)
	}

	if count == 0 {
		count = 1
	}

	for i := range listMigrations {
		if i < count {
			log.Println("start migration down " + listMigrations[i].mg.Name)

			b, err := ioutil.ReadFile(utils.RootPath() +
				"/" + cnf.Project.MigratePath + "/" + listMigrations[i].mg.Name + ".yml")
			if err != nil {
				e = goerr.New(err.Error())

				return
			}

			mg := gopsql.Migration{}
			if e = godict.YamlUnmarshal(b, &mg); e != nil {
				return
			}

			mg.Name = listMigrations[i].mg.Name

			if e = mg.Down(tx); e != nil {
				return
			}

			log.Println("success migration down " + listMigrations[i].mg.Name)
		}
	}

	errs := tx.Commit()
	if errs != nil {
		e = goerr.New(errs.Error())
	}

	return e
}
