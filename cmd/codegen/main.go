package main

import (
	"flag"
	"fmt"
	"goapi/internal/repository"
	"goapi/tools/config"
	"goapi/tools/logger"
	"goapi/utils"
	"log"
	"os"

	"github.com/cadyrov/gopsql"
)

func main() {
	var (
		domain     = flag.Bool("domain", false, "create domain")
		pSQL       = flag.Bool("psql", false, "create psql")
		core       = flag.Bool("core", false, "create core")
		controller = flag.Bool("controller", false, "create api")
		route      = flag.Bool("route", false, "create api")
		tests      = flag.Bool("tests", false, "create tests")
		name       = flag.String("name", "", "table name")
	)

	flag.Parse()

	if *name == "" {
		log.Println("set name")

		return
	}

	logger := logger.New(os.Stdout)

	env := os.Getenv("ENV")

	rootPath := utils.RootPath()

	configPath := fmt.Sprintf("%s/%s.yml", rootPath+"/resources/config", env)

	cnf, e := config.New(configPath)
	if e != nil {
		panic(e)
	}

	repo, e := repository.New(cnf.Repository, logger, false)
	if e != nil {
		panic(e)
	}

	createDomain(*name, repo, rootPath, *cnf, logger, *domain)
	createPSQL(*name, repo, rootPath, *cnf, logger, *pSQL)
	createCore(*name, repo, rootPath, *cnf, logger, *core)
	createController(*name, repo, rootPath, *cnf, logger, *controller)
	createRoute(*name, repo, rootPath, *cnf, logger, *route)
	createTests(*name, repo, rootPath, *cnf, logger, *tests)
}

func createDomain(name string, repo *repository.Repository, rootPath string,
	config config.Config, logger logger.Logger, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.DomainPath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.DomainTemplatePath)
	if err != nil {
		logger.Err(err.Error())

		return
	}

	log.Println("success create new domain: " + name)
}

func createPSQL(name string, repo *repository.Repository, rootPath string,
	config config.Config, logger logger.Logger, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.PSQLPath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.PSQLTemplatePath)
	if err != nil {
		logger.Err(err.Error())

		return
	}

	log.Println("success create new psql: " + name)
}

func createCore(name string, repo *repository.Repository, rootPath string,
	config config.Config, logger logger.Logger, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.CorePath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.CoreTemplatePath)
	if err != nil {
		logger.Err(err.Error())

		return
	}

	log.Println("success create new core: " + name)
}

func createController(name string, repo *repository.Repository, rootPath string,
	config config.Config, logger logger.Logger, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.ControllerPath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.ControllerTemplatePath)
	if err != nil {
		logger.Err(err.Error())

		return
	}

	log.Println("success create new controller: " + name)
}

func createRoute(name string, repo *repository.Repository, rootPath string,
	config config.Config, logger logger.Logger, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.RoutePath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.RouteTemplatePath)
	if err != nil {
		logger.Err(err.Error())

		return
	}

	log.Println("success create new route: " + name)
}

func createTests(tableName string, repo *repository.Repository, rootPath string, config config.Config,
	logger logger.Logger, exec bool) {
	createTestEntities(tableName, repo, rootPath, config, logger, exec)
	createCoreTest(tableName, repo, rootPath, config, logger, exec)
}

func createTestEntities(name string, repo *repository.Repository, rootPath string, config config.Config,
	logger logger.Logger, exec bool) {
	if !exec {
		return
	}

	fileName := name + "_entities"

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.TestEntitiesPath+"/"+fileName+".go",
		"public", name, rootPath+"/"+config.Generator.TestEntitiesTemplatePath)
	if err != nil {
		logger.Err(err.Error())

		return
	}

	log.Println("success create new test entities: " + fileName)
}

func createCoreTest(name string, repo *repository.Repository, rootPath string, config config.Config,
	logger logger.Logger, exec bool) {
	if !exec {
		return
	}

	fileName := name + "_test"

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.CoreTestsPath+"/"+fileName+".go",
		"public", name, rootPath+"/"+config.Generator.CoreTestsTemplatePath)
	if err != nil {
		logger.Err(err.Error())

		return
	}

	log.Println("success create new core tests: " + fileName)
}
