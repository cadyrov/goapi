package main

import (
	"context"
	"fmt"
	"goapi/api/router"
	config "goapi/tools/config"
	"goapi/tools/logger"
	"goapi/utils"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"time"

	"github.com/getsentry/sentry-go"
)

const sentryTimeout = 2

func main() {
	cnf, srv, app := initServer()

	go func() {
		if cnf.Web.SSLSertPath != "" && cnf.Web.SSLKeyPath != "" {
			if err := srv.ListenAndServeTLS(cnf.Web.SSLSertPath, cnf.Web.SSLKeyPath); err != nil {
				panic(err)
			}

			return
		}

		if err := srv.ListenAndServe(); err != nil {
			panic(err)
		}
	}()

	err := sentry.Init(sentry.ClientOptions{
		Dsn:   cnf.SentryConfig.DSN,
		Debug: cnf.SentryConfig.Debug,
	})
	if err != nil {
		panic(err)
	}

	defer sentry.Flush(sentryTimeout * time.Second)

	app.Logger.Warnf("server start on %s:%d", app.Config.Web.Host, app.Config.Web.Port)

	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt)

	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(app.Config.ReadTimeout))

	if err := srv.Shutdown(ctx); err != nil {
		cancel()

		panic(err)
	}

	app.Logger.Warn("Server stop")

	cancel()

	runtime.Goexit()
}

func initServer() (*config.Config, *http.Server, *router.App) {
	env := os.Getenv("ENV")

	configPath := fmt.Sprintf("%s/%s.yml", utils.RootPath()+"/resources/config", env)

	cnf, e := config.New(configPath)
	if e != nil {
		panic(e)
	}

	logger := logger.New(os.Stdout)

	app := router.New(logger, cnf)

	srv := &http.Server{
		Handler:      app.GetRoutes(),
		Addr:         app.Config.Web.Host + ":" + strconv.Itoa(app.Config.Web.Port),
		ReadTimeout:  time.Second * time.Duration(app.Config.Web.ReadTimeout),
		WriteTimeout: time.Second * time.Duration(app.Config.Web.WriteTimeout),
		IdleTimeout:  time.Second * time.Duration(app.Config.Web.IdleTimeout),
	}

	return cnf, srv, app
}
