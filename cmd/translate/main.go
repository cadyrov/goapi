package main

import (
	"flag"
	"fmt"
	"goapi/tools/config"
	"goapi/tools/logger"
	"goapi/utils"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/cadyrov/godict"
)

const defaultConcurrency = 10

func main() {
	concurrency := flag.Int("concurrency", defaultConcurrency, "count of routines to prepare files")

	flag.Parse()

	env := os.Getenv("ENV")
	rootPath := utils.RootPath()
	configPath := fmt.Sprintf("%s/%s.yml", rootPath+"/resources/config", env)

	cnf, e := config.New(configPath)
	if e != nil {
		panic(e)
	}

	logger := logger.New(os.Stdin)

	fileNames := getGoFiles(rootPath, []string{"vendor", "cmd"}, logger)

	result := goFiles(fileNames, *concurrency, logger)

	vocabular := make(map[string]string)

	ymlpth := rootPath + string(os.PathSeparator) + cnf.Project.I18nPath +
		string(os.PathSeparator) + strconv.Itoa(int(cnf.Project.DefaultLocale)) +
		string(os.PathSeparator) + "data.yml"

	bt, err := ioutil.ReadFile(ymlpth)
	if err != nil {
		logger.Err(err)

		return
	}

	if e = godict.YamlUnmarshal(bt, &vocabular); e != nil {
		return
	}

	for i := range result {
		if _, ok := vocabular[result[i]]; !ok {
			txt := strings.Join(strings.Split(result[i], "_"), " ")

			r := []rune(txt)

			vocabular[result[i]] = strings.ToUpper(string(r[0])) + string(r[1:])
		}
	}

	out, err := godict.YamlMarshal(vocabular)
	if err != nil {
		logger.Err(err)

		return
	}

	if err = ioutil.WriteFile(ymlpth, out, os.ModePerm); err != nil {
		logger.Err(err)

		return
	}
}

func goFiles(fileNames []string, routines int, logger logger.Logger) []string {
	result := []string{}
	resChan := make(chan []string)
	chCount := make(chan int, routines)

	for i := range fileNames {
		chCount <- 1

		go parseFileTotemplate(fileNames[i], resChan, chCount, logger)
	}

	for i := 0; i < len(fileNames); i++ {
		res := <-resChan

		result = append(result, res...)
	}

	return result
}

func parseFileTotemplate(path string, resChan chan []string, chCount chan int, logger logger.Logger) {
	res := make([]string, 0)

	var err error

	defer func() {
		if err != nil {
			logger.Err(err)
		}

		<-chCount

		resChan <- res
	}()

	logger.Infof("start parse %s", path)

	fl, err := os.Open(path)
	if err != nil {
		return
	}

	if fl == nil {
		return
	}

	defer fl.Close()

	bt, err := ioutil.ReadAll(fl)
	if err != nil {
		return
	}

	re := regexp.MustCompile(`Translatef\(ctx.Locale, ".*"`)

	res = re.FindAllString(string(bt), -1)

	for i := range res {
		res[i] = strings.ReplaceAll(res[i], `Translatef(ctx.Locale, "`, "")
		res[i] = strings.ReplaceAll(res[i], `"`, "")
	}

	logger.Infof("stop parse found %d cases", len(res))
}

func getGoFiles(path string, excludedDir []string, logger logger.Logger) (res []string) {
	fls, err := ioutil.ReadDir(path)
	if err != nil {
		logger.Err(err)

		return
	}

	for i := range fls {
		if fls[i].IsDir() {
			var excludExist bool

			for j := range excludedDir {
				if excludedDir[j] == fls[i].Name() {
					excludExist = true
				}
			}

			if excludExist {
				continue
			}

			res = append(res, getGoFiles(path+string(os.PathSeparator)+fls[i].Name(),
				excludedDir, logger)...)
		}

		if !fls[i].IsDir() {
			if strings.Contains(fls[i].Name(), ".go") {
				res = append(res, path+string(os.PathSeparator)+fls[i].Name())
			}
		}
	}

	return res
}
