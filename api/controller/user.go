package controller

import (
	"goapi/internal/domain"
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
)

func (app *App) Check(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if ctx.User == nil {
		sendError(w, goerr.New(app.I18n.Translatef(ctx.Locale,
			"unauthorized")).HTTP(http.StatusUnauthorized).
			HTTP(http.StatusUnauthorized))

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale,
		"authorize_checked"), ctx.User, nil))
}

func (app *App) UpdateUser(w http.ResponseWriter, r *http.Request) {
	_, parseIntError := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if parseIntError != nil {
		sendError(w, goerr.New(parseIntError.Error()))

		return
	}

	form := domain.UserForm{}

	parseBodyError := godict.ParseBody(r.Body, &form)
	if parseBodyError != nil {
		godict.Send(w, godict.Error(parseBodyError))

		return
	}

	ctx := app.ContextFromRequest(r)

	userEntity := domain.User{ID: ctx.User.ID, UserForm: form}

	user, e := app.Core.UpdateUser(*ctx, &userEntity)
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "update_user"), user, nil))
}

func (app *App) GetUser(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	ctx := app.ContextFromRequest(r)

	user, e := app.Core.GetUser(*ctx, &domain.User{ID: userID})
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "get_user"), user, nil))
}

func (app *App) SearchUser(w http.ResponseWriter, r *http.Request) {
	userSearchForm := &domain.UserSearchForm{}

	parseError := godict.ParseBody(r.Body, &userSearchForm)
	if parseError != nil {
		godict.Send(w, godict.Error(parseError))

		return
	}

	ctx := app.ContextFromRequest(r)

	user, pagination, e := app.Core.SearchUser(*ctx, userSearchForm)
	if e != nil {
		godict.Send(w, godict.Error(goerr.New(e.Error()).HTTP(http.StatusNotFound)))

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "search_user"),
		user, pagination))
}
