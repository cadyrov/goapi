package controller

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
)

func (app *App) GetDictionaries(w http.ResponseWriter, r *http.Request) {
	locale, err := strconv.Atoi(mux.Vars(r)["locale"])
	if err != nil {
		sendError(w, goerr.New("unable_locale"))

		return
	}

	uLocale := godict.Locale(locale)

	dictionary, e := app.Dictionary.ByLocale(uLocale)
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(uLocale, "dictionary_list"), dictionary, nil))
}
