package controller

import (
	"goapi/internal/core"
	"goapi/tools/config"
	"goapi/tools/dictionary"
	"goapi/tools/i18n"
	"goapi/tools/logger"
)

type App struct {
	Logger     logger.Logger
	Config     *config.Config
	I18n       i18n.Translator
	Dictionary *dictionary.Dictionary
	Core       *core.App
}
