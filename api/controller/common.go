package controller

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"goapi/internal/core"
	"goapi/internal/domain"
	"log"
	"net/http"
	"runtime/debug"
	"strings"
	"time"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/dgrijalva/jwt-go"
	"github.com/getsentry/sentry-go"
)

type contextKey string

const contextKeyName contextKey = "token"

var (
	ErrUnsupported        = goerr.New("some unsupported error")
	ErrDeadJWT            = errors.New("keys is dead")
	ErrNoConsistenceJWT   = errors.New("that's not even a tokenType")
	ErrIssuerNotAvailable = errors.New("issuer is not available in config")
	ErrParseJWT           = errors.New("can't parse claims")
	ErrKeyIsEmpty         = errors.New("please provide service tokenType, keys is empty")
)

func (app *App) ContextFromRequest(request *http.Request) *core.Context {
	requestContext := request.Context().Value(contextKeyName)

	if requestContext == nil {
		return &core.Context{Locale: godict.ENLocale}
	}

	coreContext := requestContext.(core.Context)

	return &coreContext
}

func (app *App) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, e := app.userByCookie(w, r)
		if e != nil {
			sendError(w, e)

			return
		}

		cn := core.Context{Locale: godict.ENLocale}

		cn.User = app.Core.PrepareUserRender(cn, *user)

		ctx := context.WithValue(r.Context(), contextKeyName, cn)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (app *App) userByCookie(w http.ResponseWriter, r *http.Request) (*domain.User, goerr.IError) {
	cookie, err := r.Cookie(app.Config.Project.Name)
	if err != nil {
		return nil, goerr.New("cookie not found").HTTP(http.StatusUnauthorized)
	}

	data, err := base64.StdEncoding.DecodeString(cookie.Value)
	if err != nil {
		return nil, goerr.New(err.Error()).HTTP(http.StatusUnauthorized)
	}

	user := app.Core.ByValidationKey(domain.ValidationKey(data))

	if user == nil {
		app.Core.DropByValidationKey(domain.ValidationKey(data))

		http.SetCookie(w, &http.Cookie{Name: app.Config.Project.Name, Value: "", MaxAge: -1})

		return nil, goerr.New("unauthorize").HTTP(http.StatusUnauthorized)
	}

	return user, nil
}

func (app *App) JwtMiddeware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tkn := r.Header.Get("Authorization")

		tokenString := parseHeader(tkn, "Bearer")

		t, err := app.ValidateJwt(tokenString)
		if err != nil {
			e := goerr.New("Check authorization error:" + err.Error()).HTTP(http.StatusForbidden)

			godict.Send(w, godict.Error(e))

			return
		}
		claims := t.Claims.(jwt.MapClaims)

		var jwtClient contextKey = "client"

		ctx := context.WithValue(r.Context(), jwtClient, claims["iss"])

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func parseHeader(token string, name string) string {
	return strings.ReplaceAll(token, name+" ", "")
}

func (app *App) CreateToken(client string) (*string, error) {
	services := app.Config.Jwt.Services
	isService := false

	for _, servname := range services {
		if servname == client {
			isService = true
		}
	}

	if !isService {
		return nil, ErrIssuerNotAvailable
	}

	claims := jwt.StandardClaims{
		ExpiresAt: time.Date(app.Config.Jwt.ExpiredAt, 10, 10, 10, 0, 0, 0, time.UTC).Unix(),
		Issuer:    client,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(app.Config.Jwt.SecretKey))
	if err != nil {
		return nil, err
	}

	return &tokenString, nil
}

func corsHeaders(responseWriter http.ResponseWriter, corsHeader string) {
	responseWriter.Header().Set("Access-Control-Allow-Origin", corsHeader)
	responseWriter.Header().Set("Access-Control-Allow-Credentials", "true")
	responseWriter.Header().Set("Access-Control-Allow-Methods", "GET,POST,PATCH,PUT,UPDATE,DELETE,OPTIONS")
	responseWriter.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Key, Authorization, Cookie")
}

func (app *App) CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
		origin := checkCorsHeaders(app, request)

		corsHeaders(responseWriter, origin)

		app.Logger.Infof("set CORS: %s", origin)

		next.ServeHTTP(responseWriter, request)
	})
}

func checkCorsHeaders(app *App, request *http.Request) string {
	corses := app.Config.Cors

	origin := request.Header.Get("Origin")

	app.Logger.Infof("incoming origin: %s", origin)

	var corsHeader string

	for i := range corses {
		if strings.Contains(corses[i], origin) {
			corsHeader = corses[i]

			return corsHeader
		}
	}

	return corsHeader
}

func (app *App) JwtBearerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := app.ValidateJwt(strings.TrimSpace(
			strings.ReplaceAll(r.Header.Get("Authorization"), "Bearer", "")))
		if err != nil {
			e := goerr.New(err.Error()).HTTP(http.StatusUnauthorized)

			godict.Send(w, godict.Error(e))

			return
		}

		next.ServeHTTP(w, r)
	})
}

// Метод не доступен.
func (app *App) NotAllowedMiddleware(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		origin := checkCorsHeaders(app, r)

		corsHeaders(w, origin)

		return
	}

	e := goerr.New("requested method is not allowed").HTTP(http.StatusMethodNotAllowed)

	godict.Send(w, godict.Error(e))
}

func (app *App) ValidateJwt(tokenString string) (*jwt.Token, error) {
	services := app.Config.Jwt.Services

	if tokenString == "" {
		return nil, ErrKeyIsEmpty
	}

	token, err := jwt.Parse(tokenString,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(app.Config.Jwt.SecretKey), nil
		})
	if err != nil {
		return nil, err
	}

	isService := false

	claims, ok := token.Claims.(jwt.MapClaims)

	if !ok {
		return nil, ErrParseJWT
	}

	if token.Valid {
		iss := claims["iss"]
		for _, servname := range services {
			if servname == iss {
				isService = true
			}
		}

		if !isService {
			return nil, ErrIssuerNotAvailable
		}

		return token, nil
	}

	if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return nil, ErrNoConsistenceJWT
		}

		if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			return nil, ErrDeadJWT
		}

		return nil, err
	}

	return nil, err
}

func (app *App) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				var e goerr.IError
				switch req := r.(type) {
				case goerr.IError:
					e = req
				case error:
					e = goerr.New(req.Error())
				case string:
					e = goerr.New(req)
				default:
					e = ErrUnsupported
				}
				key := "stack"
				message := fmt.Sprintf("%s\n%s", e.Error(), debug.Stack())
				e.PushDetail(goerr.New(key + "callback" + message))
				log.Println(e)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func (app *App) GetContext(request *http.Request) (ctx core.Context, e goerr.IError) {
	val := request.Context().Value(contextKeyName)
	if val == nil {
		e = goerr.New("user in context not found").HTTP(http.StatusNotFound)

		return
	}

	ctx = val.(core.Context)

	return
}

func sendError(w http.ResponseWriter, e goerr.IError) {
	if e.GetCode() != http.StatusBadRequest &&
		e.GetCode() != http.StatusUnauthorized {
		sentry.CaptureException(e)
	}

	godict.SendError(w, e)
}
