package controller

import (
	"goapi/internal/domain"
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
)

func (app *App) CreateGroups(w http.ResponseWriter, r *http.Request) {
	groupForm := domain.GroupsForm{}

	parseError := godict.ParseBody(r.Body, &groupForm)
	if parseError != nil {
		sendError(w, parseError)

		return
	}

	ctx := app.ContextFromRequest(r)

	groups, e := app.Core.CreateGroups(*ctx, &domain.Groups{GroupsForm: groupForm})
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "create_groups"), groups, nil))
}

func (app *App) UpdateGroups(w http.ResponseWriter, r *http.Request) {
	groupsID, parseIntError := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if parseIntError != nil {
		sendError(w, goerr.New(parseIntError.Error()))

		return
	}

	form := domain.GroupsForm{}

	parseBodyError := godict.ParseBody(r.Body, &form)
	if parseBodyError != nil {
		godict.Send(w, godict.Error(parseBodyError))

		return
	}

	groupsEntity := domain.Groups{ID: groupsID, GroupsForm: form}

	ctx := app.ContextFromRequest(r)

	groups, e := app.Core.UpdateGroups(*ctx, &groupsEntity)
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "update_groups"), groups, nil))
}

func (app *App) DeleteGroups(w http.ResponseWriter, r *http.Request) {
	groupsID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	groupsEntity := domain.Groups{ID: groupsID}

	ctx := app.ContextFromRequest(r)

	e := app.Core.DeleteGroups(*ctx, &groupsEntity)
	if e != nil {
		godict.Send(w, godict.Error(goerr.New(e.Error())))

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "delete_groups"), nil, nil))
}

func (app *App) GetGroups(w http.ResponseWriter, r *http.Request) {
	groupsID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	ctx := app.ContextFromRequest(r)

	groups, e := app.Core.GetGroups(*ctx, &domain.Groups{ID: groupsID})
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "get_groups"),
		groups, nil))
}

func (app *App) SearchGroups(w http.ResponseWriter, r *http.Request) {
	groupsSearchForm := &domain.GroupsSearchForm{}

	parseError := godict.ParseBody(r.Body, &groupsSearchForm)
	if parseError != nil {
		godict.Send(w, godict.Error(parseError))

		return
	}

	ctx := app.ContextFromRequest(r)

	groups, pagination, e := app.Core.SearchGroups(*ctx, groupsSearchForm)
	if e != nil {
		godict.Send(w, godict.Error(goerr.New(e.Error()).HTTP(http.StatusNotFound)))

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "search_groups"),
		groups, pagination))
}

func (app *App) InviteGroup(w http.ResponseWriter, r *http.Request) {
	groupID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	ctx := app.ContextFromRequest(r)

	inviteGroup := domain.InviteGroupForm{}

	parseError := godict.ParseBody(r.Body, &inviteGroup)
	if parseError != nil {
		godict.Send(w, godict.Error(parseError))

		return
	}

	e := app.Core.InviteUser(*ctx, groupID, inviteGroup)
	if e != nil {
		sendError(w, e)

		return
	}

	godict.SendOk(w, app.I18n.Translatef(ctx.Locale, "user_invited"),
		nil, nil)
}

func (app *App) InviteAccept(w http.ResponseWriter, r *http.Request) {
	groupID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	linkID, err := strconv.ParseInt(mux.Vars(r)["inviteId"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	ctx := app.ContextFromRequest(r)

	if e := app.Core.InviteGroupAccept(*ctx, groupID, linkID); e != nil {
		godict.SendError(w, e)
	}

	godict.SendOk(w, app.I18n.Translatef(ctx.Locale, "user_accept_invite"),
		nil, nil)
}

func (app *App) InviteDecline(w http.ResponseWriter, r *http.Request) {
	groupID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	linkID, err := strconv.ParseInt(mux.Vars(r)["inviteId"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	ctx := app.ContextFromRequest(r)

	if e := app.Core.InviteGroupDecline(*ctx, groupID, linkID); e != nil {
		godict.SendError(w, e)
	}

	godict.SendOk(w, app.I18n.Translatef(ctx.Locale, "user_decline_invite"),
		nil, nil)
}

func (app *App) AddRole(w http.ResponseWriter, r *http.Request) {
	groupID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	userID, err := strconv.ParseInt(mux.Vars(r)["userId"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	roles := &domain.ChangeRoleForm{}

	parseError := godict.ParseBody(r.Body, roles)
	if parseError != nil {
		godict.SendError(w, parseError)

		return
	}

	ctx := app.ContextFromRequest(r)

	if e := app.Core.AddRole(*ctx, groupID, userID, roles); e != nil {
		godict.SendError(w, e)
	}

	godict.SendOk(w, app.I18n.Translatef(ctx.Locale, "add_role"),
		nil, nil)
}

func (app *App) DropRole(w http.ResponseWriter, r *http.Request) {
	groupID, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	userID, err := strconv.ParseInt(mux.Vars(r)["userId"], 10, 64)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	roles := &domain.ChangeRoleForm{}

	parseError := godict.ParseBody(r.Body, roles)
	if parseError != nil {
		godict.SendError(w, parseError)

		return
	}

	ctx := app.ContextFromRequest(r)

	if e := app.Core.DropRole(*ctx, groupID, userID, roles); e != nil {
		godict.SendError(w, e)
	}

	godict.SendOk(w, app.I18n.Translatef(ctx.Locale, "drop_role"),
		nil, nil)
}
