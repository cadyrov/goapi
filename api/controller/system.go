package controller

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"runtime"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

func (app *App) HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	godict.Send(w, godict.Ok("Alive", nil, nil))
}

func (app *App) MemoryUsage(w http.ResponseWriter, r *http.Request) {
	var size1024 uint64 = 1024

	var m runtime.MemStats

	runtime.ReadMemStats(&m)

	report := make(map[string]string)

	report["allocated"] = fmt.Sprintf("%v KB", m.Alloc/size1024)

	report["total_allocated"] = fmt.Sprintf("%v KB", m.TotalAlloc/size1024)

	report["system"] = fmt.Sprintf("%v KB", m.Sys/size1024)

	report["garbage_collectors"] = fmt.Sprintf("%v", m.NumGC)

	godict.Send(w, godict.Ok("Memory Usage", report, nil))
}

func (app *App) Swagger(w http.ResponseWriter, r *http.Request) {
	var e goerr.IError

	if file, err := ioutil.ReadFile(app.Config.Project.SwaggerPath); err == nil {
		w.Header().Add("Content-Type", "application/json")

		_, err := w.Write(file)
		if err == nil {
			return
		}

		e = goerr.New("file cannot be write " + err.Error())
	} else {
		e = goerr.New("file cannot be read " + err.Error())
	}

	godict.Send(w, godict.Error(e))
}

func (app *App) ReinitializeCache(w http.ResponseWriter, r *http.Request) {
	app.Core.InitCache()

	godict.SendOk(w, "cache reinitialized", nil, nil)
}
