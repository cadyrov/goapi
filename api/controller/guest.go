package controller

import (
	"encoding/base64"
	"goapi/internal/domain"
	"net/http"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

const formMaxMemory = 10

func (app *App) Auth(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	ck, _ := r.Cookie(app.Config.Project.Name)
	if ck != nil {
		data, err := base64.StdEncoding.DecodeString(ck.Value)
		if err != nil {
			sendError(w, goerr.New(app.I18n.Translatef(ctx.Locale,
				"break_cookie_authorized")).HTTP(http.StatusConflict))

			return
		}

		user := app.Core.ByValidationKey(domain.ValidationKey(data))
		if user != nil {
			sendError(w, goerr.New(app.I18n.Translatef(ctx.Locale,
				"already_authorized")).HTTP(http.StatusConflict))

			return
		}
	}

	lf, e := app.prepareLoginForm(r)
	if e != nil {
		sendError(w, e)

		return
	}

	render, validationKey, e := app.Core.Login(*ctx, &lf)
	if e != nil {
		sendError(w, e)

		return
	}

	cookieBase := base64.StdEncoding.EncodeToString([]byte(validationKey))

	http.SetCookie(w, &http.Cookie{
		Name:     app.Config.Project.Name,
		Value:    cookieBase,
		SameSite: http.SameSiteNoneMode,
		Secure:   true,
		Path:     "/",
	})

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "login"), render, nil))
}

func (app *App) Logout(w http.ResponseWriter, r *http.Request) {
	cookie, _ := r.Cookie(app.Config.Project.Name)
	ctx := app.ContextFromRequest(r)

	if cookie == nil {
		sendError(w, goerr.New(app.I18n.Translatef(ctx.Locale,
			"unauthorized")).HTTP(http.StatusUnauthorized))

		return
	}

	data, err := base64.StdEncoding.DecodeString(cookie.Value)
	if err != nil {
		sendError(w, goerr.New(err.Error()).HTTP(http.StatusUnauthorized))

		return
	}

	app.Core.DropByValidationKey(domain.ValidationKey(data))

	http.SetCookie(w, &http.Cookie{Name: app.Config.Project.Name, Value: "", MaxAge: -1})

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "logout"), nil, nil))
}

func (app *App) prepareLoginForm(r *http.Request) (form domain.LoginForm, e goerr.IError) {
	formReader, err := r.MultipartReader()
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	readedForm, err := formReader.ReadForm(formMaxMemory)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	formLoginValue := readedForm.Value["login"]
	formPasswordValue := readedForm.Value["password"]
	ctx := app.ContextFromRequest(r)

	if len(formLoginValue) != 1 || len(formPasswordValue) != 1 {
		e = goerr.New(app.I18n.Translatef(ctx.Locale,
			"invalid_auth_request")).HTTP(http.StatusBadRequest)

		return
	}

	form.Login = formLoginValue[0]
	form.Password = formPasswordValue[0]

	return
}

func (app *App) ChangePassword(w http.ResponseWriter, r *http.Request) {
	form := app.prepareChangePasswordForm(r)
	ctx := app.ContextFromRequest(r)

	user, _ := app.userByCookie(w, r)
	if user != nil {
		ctx.User = &domain.UserRender{
			User: *user,
		}
	}

	err := app.Core.ChangePassword(*ctx, form)
	if err != nil {
		sendError(w, err)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "change_password"), nil, nil))
}

func (app *App) prepareChangePasswordForm(r *http.Request) (changePasswordForm *domain.ChangePasswordForm) {
	formReader, err := r.MultipartReader()
	if err != nil {
		return
	}

	readedForm, err := formReader.ReadForm(formMaxMemory)
	if err != nil {
		return
	}

	changePasswordForm = &domain.ChangePasswordForm{}
	if len(readedForm.Value["hash"]) > 0 {
		changePasswordForm.Hash = domain.ValidationKey(readedForm.Value["hash"][0])
	}

	if len(readedForm.Value["password"]) > 0 {
		changePasswordForm.Password = readedForm.Value["password"][0]
	}

	if len(readedForm.Value["confirm"]) > 0 {
		changePasswordForm.Confirm = readedForm.Value["confirm"][0]
	}

	return
}

func (app *App) PasswordRecovery(w http.ResponseWriter, r *http.Request) {
	formReader, err := r.MultipartReader()
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	passRecoveryForm, err := formReader.ReadForm(formMaxMemory)
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	ctx := app.ContextFromRequest(r)

	email, ok := passRecoveryForm.Value["login"]
	if !ok || len(email) == 0 {
		sendError(w, goerr.New(app.I18n.Translatef(ctx.Locale,
			"invalid_auth_request")).HTTP(http.StatusUnauthorized))

		return
	}

	url, ok := passRecoveryForm.Value["url"]
	if !ok || len(email) == 0 {
		sendError(w, goerr.New(app.I18n.Translatef(ctx.Locale,
			"invalid_auth_request")).HTTP(http.StatusUnauthorized))

		return
	}

	_, e := app.Core.PasswordRecovery(*ctx, email[0], url[0])
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "pass_recovery"), nil, nil))
}

func (app *App) Register(w http.ResponseWriter, r *http.Request) {
	formReader, err := r.MultipartReader()
	if err != nil {
		return
	}

	passRecoveryForm, err := formReader.ReadForm(formMaxMemory)
	if err != nil {
		return
	}

	ctx := app.ContextFromRequest(r)

	email, ok := passRecoveryForm.Value["login"]
	if !ok || len(email) == 0 {
		sendError(w, goerr.New(app.I18n.Translatef(ctx.Locale,
			"invalid_auth_request")).HTTP(http.StatusUnauthorized))

		return
	}

	_, e := app.Core.Register(*ctx, email[0])
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "user_registered"), nil, nil))
}
