package router

import (
	"goapi/internal/domain"
	"net/http"

	"github.com/gorilla/mux"
)

// Form CreateUserRequest
//
// swagger:parameters CreateUserRequest
type CreateUserRequest struct {
	// in: body
	// required: true
	Body struct {
		domain.UserForm
	}
}

// Form UpdateUserRequest
//
// swagger:parameters UpdateUserRequest
type UpdateUserRequest struct {
	// ID of updating User
	// Required: true
	// in: path
	ID string `json:"id"`
	// in: body
	// required: true
	Body struct {
		domain.UserForm
	}
}

// Form DeleteUserRequest
//
// swagger:parameters DeleteUserRequest
type DeleteUserRequest struct {
	// ID of deleting User
	// Required: true
	// in: path
	ID string `json:"id"`
}

// Form GetUserRequest
//
// swagger:parameters GetUserRequest
type GetUserRequest struct {
	// ID of User
	// Required: true
	// in: path
	ID string `json:"id"`
}

// Form SearchUserRequest
//
// swagger:parameters SearchUserRequest
type SearchUserRequest struct {
	// in: body
	// required: true
	Body struct {
		domain.UserSearchForm
	}
}

// User response
//
// swagger:response UserResponse
type UserResponse struct {
	// In: body
	Body struct {
		Message string            `json:"message"`
		Data    domain.UserRender `json:"data"`
	}
}

// UserListResponse response
//
// swagger:response UserListResponse
type UserListResponse struct {
	// In: body
	Body struct {
		Message string               `json:"message"`
		Data    []*domain.UserRender `json:"data"`
	}
}

func (app *App) SetUserRoutes(router *mux.Router) {
	updateUser(app, router)
	getUser(app, router)
	searchUser(app, router)
}

func updateUser(app *App, router *mux.Router) {
	// swagger:route PUT /v1/user/{id} User UpdateUserRequest
	//
	// Update
	//
	// Update User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}", app.UpdateUser).Methods(http.MethodPut)
}

func getUser(app *App, router *mux.Router) {
	// swagger:route GET /v1/user/{id}  User GetUserRequest
	//
	// Get
	//
	// Get User
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id}", app.GetUser).Methods(http.MethodGet)
}

func searchUser(app *App, router *mux.Router) {
	// swagger:route POST /v1/user User SearchUserRequest
	//
	// Search
	//
	// Search User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserListResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.SearchUser).Methods(http.MethodPost)
}
