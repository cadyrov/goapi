package router

import (
	"goapi/api/controller"
	"goapi/internal/cache"
	"goapi/internal/core"
	"goapi/internal/repository"
	"goapi/tools/config"
	"goapi/tools/dictionary"
	"goapi/tools/i18n"
	"goapi/tools/logger"
	"goapi/tools/mailgun"
	"net/http"

	"github.com/gorilla/mux"
)

type App struct {
	controller.App
}

func New(l logger.Logger, c *config.Config) *App {
	translator, e := i18n.New(c.Project.DefaultLocale, c, l)
	if e != nil {
		panic(e)
	}

	dict, e := dictionary.New(c.Project.DefaultLocale, c, l)
	if e != nil {
		panic(e)
	}

	repo, e := repository.New(c.Repository, l, false)
	if e != nil {
		panic(e)
	}

	che, e := cache.New(l)
	if e != nil {
		panic(e)
	}

	malgn := mailgun.NewMailGunService(c.Mailgun)

	cr := core.New(c, repo, l, translator, dict, nil, che, malgn)

	cr.InitCache()

	return &App{
		controller.App{
			Logger:     l,
			Config:     c,
			I18n:       translator,
			Dictionary: dict,
			Core:       cr,
		},
	}
}

func (app *App) GetRoutes() *mux.Router {
	routes := mux.NewRouter()
	routes.Use(app.LoggingMiddleware)
	routes.Use(app.CorsMiddleware)
	routes.MethodNotAllowedHandler = http.HandlerFunc(app.NotAllowedMiddleware)

	mainRoute := routes.PathPrefix("/api").Subrouter()

	system := mainRoute.PathPrefix("/system").Subrouter()
	app.SetSystemRoutes(system)

	guest := mainRoute.PathPrefix("/guest").Subrouter()
	app.SetGuestRoutes(guest)

	dictionaryRoute := mainRoute.PathPrefix("/dictionaries").Subrouter()
	app.SetDictionariesRoutes(dictionaryRoute)

	v1Route := mainRoute.PathPrefix("/v1").Subrouter()
	v1Route.Use(app.AuthMiddleware)

	check := v1Route.PathPrefix("/check").Subrouter()
	checkHandle(app, check)

	logout := v1Route.PathPrefix("/logout").Subrouter()
	logoutHandle(app, logout)

	user := v1Route.PathPrefix("/user").Subrouter()
	app.SetUserRoutes(user)

	groups := v1Route.PathPrefix("/groups").Subrouter()
	app.SetGroupsRoutes(groups)

	return routes
}

func checkHandle(app *App, router *mux.Router) {
	// swagger:route GET /v1/check User CheckCookie
	//
	// CheckCookie
	//
	// Cookie name = Project.Name
	// Cookie life time = 24 hours
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       409: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.Check).Methods(http.MethodGet)
}

func logoutHandle(app *App, router *mux.Router) {
	// swagger:route GET /v1/logout User
	//
	// Logout
	//
	// Logout User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: description: Empty
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.Logout).Methods(http.MethodGet)
}
