package router

import (
	"net/http"

	"github.com/gorilla/mux"
)

// Form
//
// swagger:parameters PasswordRecoveryForm RegisterForm
type PasswordRecoveryForm struct {
	// Логин пользователя - email
	// Required: true
	// In: formData
	// name: login
	Login string `json:"login"`
	// Url ссылки для письма
	// Required: true
	// In: formData
	// name: login
	URL string `json:"url"`
}

// Form
//
// swagger:parameters LoginForm
type LoginForm struct {
	// Логин пользователя - email
	// Required: true
	// In: formData
	// name: login
	Login string `json:"login"`
	// Пароль пользователя. Минимальная длина 6 символов
	// Required: true
	// In: formData
	// name: password
	// min length: 6
	Password string `json:"password"`
}

// Form
//
// swagger:parameters ChangePasswordForm
type ChangePasswordForm struct {
	// Подтверждение пароля
	// Required: true
	// In: formData
	// name: confirm
	Confirm string `json:"confirm"`
	// Пароль пользователя. Минимальная длина 6 символов
	// Required: true
	// In: formData
	// name: password
	// min length: 6
	Password string `json:"password"`
	// Хэш восстановления необязателен если пользователь авторизован
	// In: formData
	// name: hash
	// min length: 6
	Hash string `json:"hash"`
}

// Response PasswordRecoveryResponse
//
// swagger:response PasswordRecoveryResponse
type PasswordRecoveryResponse struct {
	// In: body
	Body struct {
		Message string `json:"message"`
		Data    string `json:"data"`
	}
}

// Response ChangePasswordResponse
//
// swagger:response ChangePasswordResponse
type ChangePasswordResponse struct {
	// In: body
	Body struct {
		Message string `json:"message"`
		Data    string `json:"data"`
	}
}

func (app *App) SetGuestRoutes(router *mux.Router) {
	auth(app, router)
	recoveryPassword(app, router)
	changePassword(app, router)
	register(app, router)
}

func auth(app *App, router *mux.Router) {
	// swagger:route POST /guest/auth Auth LoginForm
	//
	// Login
	//
	// Post Login
	// Cookie name = Project.Name (goapi)
	// Cookie life time = 24 hours
	//
	//     Consumes:
	//     - multipart/form-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       409: ResponseError
	//       500: ResponseError
	router.HandleFunc("/auth", app.Auth).Methods(http.MethodPost)
}

func register(app *App, router *mux.Router) {
	// swagger:route POST /guest/register Auth RegisterForm
	//
	// Register
	//
	// Post Register
	//
	//     Consumes:
	//     - multipart/form-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       409: ResponseError
	//       500: ResponseError
	router.HandleFunc("/register", app.Register).Methods(http.MethodPost)
}

func recoveryPassword(app *App, router *mux.Router) {
	// swagger:route POST /guest/recovery-password Auth PasswordRecoveryForm
	//
	// PasswordRecovery
	//
	// POST PasswordRecovery returns 20-char hash in data
	//
	//     Consumes:
	//     - multipart/form-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: PasswordRecoveryResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       409: ResponseError
	//       500: ResponseError
	router.HandleFunc("/recovery-password", app.PasswordRecovery).Methods(http.MethodPost)
}

func changePassword(app *App, router *mux.Router) {
	// swagger:route POST /guest/change-password Auth ChangePasswordForm
	//
	// ChangePasswordForm
	//
	// POST ChangePassword
	// Change password steps:
	// 1. Recovery password call /v1/guest/recovery-password
	// 2. Change password with hash, password, confirmation password
	//
	//     Consumes:
	//     - multipart/form-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: description: Empty
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       409: ResponseError
	//       500: ResponseError
	router.HandleFunc("/change-password", app.ChangePassword).Methods(http.MethodPost)
}
