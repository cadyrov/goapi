package router

import (
	"net/http"

	"github.com/gorilla/mux"
)

// swagger:model
type GoErr struct {
	// Service status message
	// Required: true
	// Example: permission denied
	Message string `json:"message,omitempty"`
	// Identity
	// Required: true
	// Example: 500
	Code string `json:"code,omitempty"`
}

// Error response format
//
// swagger:response ResponseError
type ResponseError struct {
	// In: body
	Body struct {
		// Error message
		// Required: true
		Error GoErr `json:"error"`
	}
}

// Response when requesting the status of the service
//
// swagger:response ResponseMessage
type ResponseMessage struct {
	// In: body
	Body struct {
		// Service status message
		// Required: true
		// Example: Success
		Message string `json:"message,omitempty"`
	}
}

// Response when requesting the status of the service
//
// swagger:response ResponseMemoryUsage
type ResponseMemoryUsage struct {
	// In: body
	Body struct {
		// Service status message
		// Required: true
		// Example: Memory usage
		Message string `json:"message,omitempty"`
		// Response data
		// Required: true
		Data struct {
			// Amount of allocated memory
			// Required: true
			// Example: 100 KB
			Allocated string `json:"allocated,omitempty"`
			// Amount of allocated memory
			// Required: true
			// Example: 300 KB
			TotalAllocated string `json:"total_allocated,omitempty"`
			// Amount of allocated memory
			// Required: true
			// Example: 200 KB
			System string `json:"system,omitempty"`
			// Amount of allocated memory
			// Required: true
			// Example: 1
			GarbageCollectors string `json:"garbage_collectors,omitempty"`
		} `json:"data"`
	}
}

type RequestWithCookie struct {
	// Service cookies required
	// In: Cookie
	// Required: true
	Gauth string
}

// swagger:parameters FileDeleteId
type RequestByIDWithCookie struct {
	// RedirectURL identifier
	// Required: true
	// in: path
	ID string `json:"id"`
	// Service cookies required
	// In: Cookie
	// Required: true
	Gauth string
}

type RequestWithBearerToken struct {
	// Service bearer token required
	// In: Authorization
	// Required: true
	Bearer string
}

// swagger:parameters FileDeleteId
type RequestByIDWithBearerToken struct {
	// RedirectURL identifier
	// Required: true
	// in: path
	ID string `json:"id"`
	// Service bearer token required
	// In: header
	// Required: true
	Bearer string
}

func (app *App) SetSystemRoutes(router *mux.Router) {
	// swagger:route GET /system/health System HealthCheckHandler
	//
	// Service availability
	//
	// Shows service availability
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	router.HandleFunc("/health", app.HealthCheckHandler).Methods(http.MethodGet)
	// swagger:route GET /system/memory System MemoryUsage
	//
	// Memory usage statistics
	//
	// How much memory does the application use?
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMemoryUsage
	router.HandleFunc("/memory", app.MemoryUsage).Methods(http.MethodGet)
	// swagger:route GET /system/swagger Swagger system
	//
	// Swagger
	//
	// Return swagger
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMemoryUsage
	router.HandleFunc("/swagger", app.Swagger).Methods(http.MethodGet)
	app.dropCache(router)
}

func (app *App) dropCache(router *mux.Router) {
	// swagger:route DELETE /system/cache Swagger
	//
	// Drop cache
	//
	// Drop cache and reinitialize
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	router.HandleFunc("/cache", app.ReinitializeCache).Methods(http.MethodDelete)
}
