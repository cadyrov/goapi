package router

import (
	"net/http"

	"github.com/cadyrov/godict"
	"github.com/gorilla/mux"
)

// Form locale
//
// swagger:parameters DictionaryForm
type DictionaryForm struct {
	// Locale identifier
	// Required: true
	// in: path
	Locale string `json:"locale"`
}

// Dictionary response
//
// swagger:response DictionaryRender
type DictionaryRender struct {
	// In: body
	Body struct {
		Message string            `json:"message"`
		Data    godict.Dictionary `json:"data"`
	}
}

func (app *App) SetDictionariesRoutes(router *mux.Router) {
	// swagger:route GET /dictionaries/{locale} Dictionaries DictionaryForm
	//
	// Get Dictionary
	//
	// Get Dictionary
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: DictionaryRender
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{locale}", app.GetDictionaries).Methods(http.MethodGet)
}
