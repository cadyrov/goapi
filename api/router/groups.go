package router

import (
	"goapi/internal/domain"
	"net/http"

	"github.com/gorilla/mux"
)

// Form InviteGroupsRequest
//
// swagger:parameters InviteGroupsRequest
type InviteGroupsRequest struct {
	// ID of updating Groups
	// Required: true
	// in: path
	GroupsID string `json:"id"`
	// in: body
	// required: true
	Body struct {
		domain.InviteGroupForm
	}
}

// Form CreateGroupsRequest
//
// swagger:parameters CreateGroupsRequest
type CreateGroupsRequest struct {
	// in: body
	// required: true
	Body struct {
		domain.GroupsForm
	}
}

// Form UpdateGroupsRequest
//
// swagger:parameters UpdateGroupsRequest
type UpdateGroupsRequest struct {
	// ID of updating Groups
	// Required: true
	// in: path
	GroupsID string `json:"id"`
	// in: body
	// required: true
	Body struct {
		domain.GroupsForm
	}
}

// Form InviteGroupsRequest
//
// swagger:parameters AddRoleRequest DropRoleRequest
type TeamRoleRequest struct {
	// ID of updating Groups
	// Required: true
	// in: path
	GroupsID string `json:"id"`
	// ID of updating User
	// Required: true
	// in: path
	UserID string `json:"userId"`
	// in: body
	// required: true
	Body struct {
		domain.ChangeRoleForm
	}
}

// Form InviteOperationGroupsRequest
//
// swagger:parameters InviteAcceptGroupsRequest InviteDeclineGroupsRequest
type InviteOperationGroupsRequest struct {
	// ID of updating Groups
	// Required: true
	// in: path
	GroupsID string `json:"id"`
	// ID of updating invite
	// Required: true
	// in: path
	InviteID string `json:"inviteId"`
}

// Form DeleteGroupsRequest
//
// swagger:parameters DeleteGroupsRequest
type DeleteGroupsRequest struct {
	// ID of deleting Groups
	// Required: true
	// in: path
	GroupsID string `json:"id"`
}

// Form GetGroupsRequest
//
// swagger:parameters GetGroupsRequest
type GetGroupsRequest struct {
	// ID of Groups
	// Required: true
	// in: path
	GroupsID string `json:"id"`
}

// Form SearchGroupsRequest
//
// swagger:parameters SearchGroupsRequest
type SearchGroupsRequest struct {
	// in: body
	// required: true
	Body struct {
		domain.GroupsSearchForm
	}
}

// Groups response
//
// swagger:response GroupsResponse
type GroupsResponse struct {
	// In: body
	Body struct {
		Message string              `json:"message"`
		Data    domain.GroupsRender `json:"data"`
	}
}

// GroupsListResponse response
//
// swagger:response GroupsListResponse
type GroupsListResponse struct {
	// In: body
	Body struct {
		Message string                 `json:"message"`
		Data    []*domain.GroupsRender `json:"data"`
	}
}

func (app *App) SetGroupsRoutes(router *mux.Router) {
	createGroups(app, router)
	updateGroups(app, router)
	deleteGroups(app, router)
	getGroups(app, router)
	searchGroups(app, router)
	inviteAccept(app, router)
	inviteDecline(app, router)
	invite(app, router)
	addRole(app, router)
	dropRole(app, router)
}

func addRole(app *App, router *mux.Router) {
	// swagger:route POST /v1/groups/{id}/add-role/{userId} Groups AddRoleRequest
	//
	// Добавить роль для пользователя
	//
	// Добавить роль для пользователя может только человек с правом удаления ролей группы
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}/add-role/{userId:[0-9]+}", app.AddRole).Methods(http.MethodPost)
}

func dropRole(app *App, router *mux.Router) {
	// swagger:route POST /v1/groups/{id}/drop-role/{userId} Groups DropRoleRequest
	//
	// Удалить роль для пользователя
	//
	// Удалить роль для пользователя может только человек с правом удаления ролей группы
	// При этом не может быть удалена последняя такая роль
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}/drop-role/{userId:[0-9]+}", app.DropRole).Methods(http.MethodPost)
}

func inviteAccept(app *App, router *mux.Router) {
	// swagger:route PUT /v1/groups/{id}/invite/{inviteId} Groups InviteAcceptGroupsRequest
	//
	// Принять приглашение только если у него статус состояния новое
	//
	// Принять приглашение
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}/invite/{inviteId:[0-9]+}", app.InviteAccept).Methods(http.MethodPut)
}

func inviteDecline(app *App, router *mux.Router) {
	// swagger:route DELETE /v1/groups/{id}/invite/{inviteId} Groups InviteDeclineGroupsRequest
	//
	// Отклонить приглашение
	//
	// Отклонить приглашение может создатель и тот кому адресовано
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}/invite/{inviteId:[0-9]+}", app.InviteDecline).Methods(http.MethodDelete)
}

func invite(app *App, router *mux.Router) {
	// swagger:route POST /v1/groups/{id}/invite Groups InviteGroupsRequest
	//
	// Приглашение пользователя
	//
	// Создается запись приглашения пользователя в группу
	// если пользователь существует  привязывается если нет  -
	// ждет пока пользователь будет зарегистрирован и в этот
	// момент выполняется привязка на почтовый ящик отправляется уведомление
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}/invite", app.InviteGroup).Methods(http.MethodPost)
}

func createGroups(app *App, router *mux.Router) {
	// swagger:route POST /v1/groups/create Groups CreateGroupsRequest
	//
	// Создание группы - в данном контексте группа = организация от лица которой ведется деятельность
	//
	// Одновременно создается группа и сущность связи пользователя с группой
	// пользователь создавший группу имеет в ней все права
	// (добавление прав, редактирование справочников, передача прав ,
	//  приглашение пользователей и исключение их )
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: GroupsResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/create", app.CreateGroups).Methods(http.MethodPost)
}

func updateGroups(app *App, router *mux.Router) {
	// swagger:route PUT /v1/groups/{id} Groups UpdateGroupsRequest
	//
	// Update
	//
	// Update Groups
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: GroupsResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}", app.UpdateGroups).Methods(http.MethodPut)
}

func deleteGroups(app *App, router *mux.Router) {
	// swagger:route DELETE /v1/groups/{id}  Groups DeleteGroupsRequest
	//
	// Delete
	//
	// Delete реально группа блокируется но не удаляется
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id}", app.DeleteGroups).Methods(http.MethodDelete)
}

func getGroups(app *App, router *mux.Router) {
	// swagger:route GET /v1/groups/{id}  Groups GetGroupsRequest
	//
	// Get
	//
	// Get Groups
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: GroupsResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id}", app.GetGroups).Methods(http.MethodGet)
}

func searchGroups(app *App, router *mux.Router) {
	// swagger:route POST /v1/groups Groups SearchGroupsRequest
	//
	// Search
	//
	// Search Groups
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: GroupsListResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.SearchGroups).Methods(http.MethodPost)
}
