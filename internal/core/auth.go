package core

import (
	"goapi/internal/domain"
	"goapi/utils"
	"net/http"
	"time"

	"github.com/cadyrov/goerr"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

const hashLenght = 20

func (app *App) ChangePassword(ctx Context, changePasswordForm *domain.ChangePasswordForm) goerr.IError {
	var user *domain.User

	if ctx.User != nil {
		e := changePasswordForm.Validate(false)
		if e != nil {
			return e
		}

		user = &ctx.User.User
	} else {
		e := changePasswordForm.Validate(true)
		if e != nil {
			return e
		}

		vkey := string(changePasswordForm.Hash)

		user, e = app.repository.UserAI.ByHash(&domain.User{PasswordHash: &vkey}, nil)
		if e != nil {
			return e
		}
	}

	if user == nil {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found"))
	}

	if user.StatusID == domain.UserStatusBlock {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_is_blocked"))
	}

	bt, err := bcrypt.GenerateFromPassword([]byte(changePasswordForm.Password), bcrypt.DefaultCost)
	if err != nil {
		return goerr.New(err.Error())
	}

	newPass := string(bt)
	user.PasswordHash = &newPass

	user.StatusID = domain.UserStatusActive

	_, e := app.repository.UserAI.Save(user, nil)
	if e != nil {
		return e
	}

	app.cache.AuthAI.Set(user)

	return app.updateInvitesToUser(user)
}

func (app *App) updateInvitesToUser(user *domain.User) goerr.IError {
	links, _, e := app.repository.UserLinkGroupAI.Search(&domain.UserLinkGroupSearchForm{
		Email:    user.Email,
		Statuses: []int{domain.InviteStatusNew},
	}, nil)

	if e != nil {
		return e
	}

	for i := range links {
		if links[i].UserID == nil {
			continue
		}

		links[i].UserID = &user.ID

		if _, err := app.repository.UserLinkGroupAI.Save(links[i], nil); err != nil {
			return err
		}
	}

	return nil
}

func (app *App) Login(ctx Context, loginForm *domain.LoginForm) (
	render *domain.UserRender, validationKey domain.ValidationKey, e goerr.IError) {
	us := &domain.User{UserForm: domain.UserForm{Email: loginForm.Login}}
	user := app.cache.AuthAI.ByEmail(us)

	if user.StatusID != domain.UserStatusActive {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_active")).HTTP(http.StatusBadRequest)

		return
	}

	if user.PasswordHash == nil {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "password_not_exists")).HTTP(http.StatusConflict)

		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(*user.PasswordHash), []byte(loginForm.Password))
	if err != nil {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "authorization_failed")).HTTP(http.StatusUnauthorized)

		return
	}

	render = app.PrepareUserRender(ctx, *user)

	rString, err := utils.RString(app.config.Jwt.KeyLength)
	if err != nil {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "authorization_failed"))

		return
	}

	token, e := app.createJwtWithRandomString(rString)
	if e != nil {
		return
	}

	app.cache.AuthAI.AddOnKey(domain.ValidationKey(token), render.ID)

	return render, domain.ValidationKey(token), e
}

func (app *App) createJwtWithRandomString(randomString string) (string, goerr.IError) {
	expiredAt := time.Now().Add(time.Hour * time.Duration(app.config.Security.CookieLiveHours))

	claim := jwt.StandardClaims{
		ExpiresAt: expiredAt.Unix(),
		Id:        randomString,
		Issuer:    app.config.Project.Name,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)

	tokenString, err := token.SignedString([]byte(app.config.Jwt.SecretKey))
	if err != nil {
		return "", goerr.New(err.Error())
	}

	return tokenString, nil
}

func (app *App) ByValidationKey(key domain.ValidationKey) *domain.User {
	return app.cache.AuthAI.ByValidationKey(key)
}

func (app *App) DropByValidationKey(key domain.ValidationKey) {
	app.cache.AuthAI.DropValidationKey(key)
}

func (app *App) PasswordRecovery(ctx Context, login string, url string) (hash string, e goerr.IError) {
	users, _, e := app.repository.UserAI.Search(&domain.UserSearchForm{Email: login}, nil)
	if e != nil {
		return
	}

	if len(users) != 1 {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found"))

		return
	}

	if users[0].StatusID == domain.UserStatusBlock {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "user_is_blocked"))

		return
	}

	hash, err := utils.RString(hashLenght)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	users[0].PasswordHash = &hash

	newUser, e := app.repository.UserAI.Save(users[0], nil)
	if e != nil {
		return
	}

	app.cache.AuthAI.Set(newUser)

	msg := app.mailgun.NewMessage(
		MailSender,
		app.i18n.Translatef(ctx.Locale, "mail_recovery_subject"),
		app.i18n.Translatef(ctx.Locale, "mail_recovery_body", url, hash),
		newUser.Email,
	)

	if _, _, e = app.mailgun.Send(msg); e != nil {
		return
	}

	return hash, nil
}

func (app *App) Register(ctx Context, login string) (hash string, e goerr.IError) {
	users, _, e := app.repository.UserAI.Search(&domain.UserSearchForm{Email: login}, nil)
	if e != nil {
		return
	}

	if len(users) != 0 {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "user_registered_yet"))

		return
	}

	hash, err := utils.RString(hashLenght)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	user := domain.User{
		PasswordHash: &hash,
		Roles:        []string{},
		StatusID:     domain.UserStatusNew,
		UserForm: domain.UserForm{
			Username: login,
			Email:    login,
		},
	}

	newUser, e := app.repository.UserAI.Save(&user, nil)
	if e != nil {
		return
	}

	app.cache.AuthAI.Set(newUser)

	msg := app.mailgun.NewMessage(
		MailSender,
		app.i18n.Translatef(ctx.Locale, "mail_new_subject"),
		app.i18n.Translatef(ctx.Locale, "mail_new_body", hash),
		newUser.Email,
	)

	if _, _, e = app.mailgun.Send(msg); e != nil {
		return
	}

	return hash, nil
}
