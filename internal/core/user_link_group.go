package core

import (
	"goapi/internal/domain"
	"goapi/internal/repository/access"
	"net/http"
	"time"

	"github.com/cadyrov/goerr"
)

func (app *App) createGroupLinks(ulg *domain.UserLinkGroup, tx access.Transacter) (
	*domain.UserLinkGroup, goerr.IError) {
	ulg.CreatedAt = time.Now()

	e := ulg.Validate()
	if e != nil {
		return nil, e
	}

	entity, e := app.repository.UserLinkGroupAI.Save(ulg, tx)
	if e != nil {
		return nil, e
	}

	return entity, nil
}

func (app *App) canRightGroup(ctx Context, userID int64,
	groupID int64, tx access.Transacter) goerr.IError {
	roleMap := make(map[int64]struct{})

	roleMap[domain.GroupRoleRights] = struct{}{}

	return app.hasAnyRole(ctx, userID, groupID, roleMap, tx)
}

func (app *App) canChangeGroup(ctx Context, userID int64,
	groupID int64, tx access.Transacter) goerr.IError {
	roleMap := make(map[int64]struct{})

	roleMap[domain.GroupRoleChange] = struct{}{}

	return app.hasAnyRole(ctx, userID, groupID, roleMap, tx)
}

func (app *App) canInviteGroup(ctx Context, userID int64,
	groupID int64, tx access.Transacter) goerr.IError {
	roleMap := make(map[int64]struct{})

	roleMap[domain.GroupRoleInvite] = struct{}{}

	return app.hasAnyRole(ctx, userID, groupID, roleMap, tx)
}

func (app *App) hasAnyRole(ctx Context, userID int64,
	groupID int64, roles map[int64]struct{}, tx access.Transacter) goerr.IError {
	ulgList, _, e := app.repository.UserLinkGroupAI.Search(
		&domain.UserLinkGroupSearchForm{
			GroupIDs: []int64{groupID},
			UserIDs:  []int64{userID},
			Statuses: []int{domain.InviteStatusAccept},
		}, tx)
	if e != nil {
		return e
	}

	if len(ulgList) > 1 {
		return goerr.New(app.i18n.Translatef(ctx.Locale,
			"so_many_links")).HTTP(http.StatusConflict)
	}

	if len(ulgList) < 1 {
		return goerr.New(app.i18n.Translatef(ctx.Locale,
			"links not found")).HTTP(http.StatusNotFound)
	}

	if len(roles) == 0 {
		return nil
	}

	for i := range ulgList[0].Roles {
		if _, ok := roles[ulgList[0].Roles[i]]; ok {
			return nil
		}
	}

	return goerr.New(app.i18n.Translatef(ctx.Locale,
		"forbidden")).HTTP(http.StatusForbidden)
}
