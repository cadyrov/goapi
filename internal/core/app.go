package core

import (
	"goapi/internal/cache"
	"goapi/internal/domain"
	"goapi/internal/repository"
	"goapi/tools/config"
	"goapi/tools/dictionary"
	"goapi/tools/i18n"
	"goapi/tools/logger"
	"goapi/tools/mailer"
	"goapi/tools/mailgun"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

type App struct {
	config     *config.Config
	repository *repository.Repository
	logger     logger.Logger
	i18n       i18n.Translator
	dictionary *dictionary.Dictionary
	mailer     *mailer.Mailer
	cache      *cache.Cache
	mailgun    *mailgun.MailGun
}

func New(config *config.Config, repository *repository.Repository,
	logger logger.Logger, i18n i18n.Translator, dictionary *dictionary.Dictionary,
	mailer *mailer.Mailer, cache *cache.Cache, mailgun *mailgun.MailGun) *App {
	return &App{
		config:     config,
		repository: repository,
		logger:     logger,
		i18n:       i18n,
		dictionary: dictionary,
		mailer:     mailer,
		cache:      cache,
		mailgun:    mailgun,
	}
}

type Context struct {
	Locale godict.Locale
	User   *domain.UserRender
}

const (
	UserStatusDictionary = "userStatus"
	MailSender           = "authorization@goapi.online"
)

func (app *App) UserStatus(ctx Context, dictionaryID int) (
	render godict.DictionaryRender, e goerr.IError) {
	return app.dictionaryRender(ctx, UserStatusDictionary, dictionaryID)
}

func (app *App) dictionaryRender(ctx Context, dictionaryName string, dictionaryID int) (
	render godict.DictionaryRender, e goerr.IError) {
	dct, e := app.dictionary.ByLocale(ctx.Locale)
	if e != nil {
		return
	}

	return dct.DictionaryRender(dictionaryName, dictionaryID)
}

func (app *App) InitCache() {
	app.logger.Info("core: start init cache")

	app.initCacheUser()
}

func (app *App) initCacheUser() {
	app.logger.Info("core:  start init cache user")

	if app.repository == nil {
		app.logger.Err("core:  repository is not initialize")
	}

	if app.cache == nil {
		app.logger.Err("core:  cache is is not initialize")
	}

	users, _, err := app.repository.UserAI.Search(&domain.UserSearchForm{}, nil)
	if err != nil {
		app.logger.Err(err)
	}

	app.cache.AuthAI.SetUsers(users)

	app.logger.Info("core:  user cache initialized")
}
