package core

import (
	"goapi/internal/domain"
	"goapi/utils"
	"net/http"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

func (app *App) UpdateUser(ctx Context, user *domain.User) (
	*domain.UserRender, goerr.IError) {
	oldData, e := app.repository.UserAI.ByID(user, nil)
	if e != nil {
		return nil, e
	}

	oldData.Username = user.Username
	oldData.Email = user.Email

	if e := oldData.Validate(); e != nil {
		return nil, e
	}

	_, pagination, e := app.repository.UserAI.Search(&domain.UserSearchForm{
		Email:      user.Email,
		SearchForm: domain.SearchForm{ExcludedIDs: []int64{oldData.ID}},
	}, nil)
	if e != nil {
		return nil, e
	}

	if pagination.Total > 0 {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_with_email_exist")).HTTP(http.StatusBadRequest)
	}

	userEntity, e := app.repository.UserAI.Save(oldData, nil)
	if e != nil {
		return nil, e
	}

	userRender := app.PrepareUserRender(ctx, *userEntity)

	app.cache.AuthAI.Set(user)

	return userRender, nil
}

func (app *App) GetUser(ctx Context, user *domain.User) (
	*domain.UserRender, goerr.IError) {
	userEntity, e := app.repository.UserAI.ByID(user, nil)
	if e != nil {
		return nil, e
	}

	if userEntity == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found")).HTTP(http.StatusNotFound)
	}

	userRender := app.PrepareUserRender(ctx, *userEntity)

	return userRender, nil
}

func (app *App) SearchUser(ctx Context, searchUser *domain.UserSearchForm) (
	[]*domain.UserRender, godict.Pagination, goerr.IError) {
	users, pagination, e := app.repository.UserAI.Search(searchUser, nil)
	if e != nil {
		return nil, pagination, e
	}

	renders := make([]*domain.UserRender, len(users))

	for i := range users {
		renders = append(renders, app.PrepareUserRender(ctx, *users[i]))
	}

	return renders, pagination, nil
}

func (app *App) PrepareUserRender(ctx Context, userEntity domain.User) *domain.UserRender {
	userEntity.PasswordHash = utils.StringPtr("")

	dct, e := app.UserStatus(ctx, int(userEntity.StatusID))
	if e != nil {
		app.logger.Err(e)
	}

	return &domain.UserRender{
		User:   userEntity,
		Status: dct,
	}
}
