package core

import (
	"goapi/internal/domain"
	"goapi/utils"
	"net/http"
	"time"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

func (app *App) InviteGroupAccept(ctx Context, groupID int64, linkID int64) goerr.IError {
	link, e := app.linkToChange(ctx, groupID, linkID)
	if e != nil {
		return e
	}

	if link.UserID == nil {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_set")).HTTP(http.StatusConflict)
	}

	if *link.UserID != ctx.User.ID {
		return goerr.New(app.i18n.Translatef(ctx.Locale,
			"its_arent_your_link")).HTTP(http.StatusForbidden)
	}

	link.Status = domain.InviteStatusAccept

	if _, e = app.repository.UserLinkGroupAI.Save(link, nil); e != nil {
		return e
	}

	return nil
}

func (app *App) InviteGroupDecline(ctx Context, groupID int64, linkID int64) goerr.IError {
	link, e := app.linkToChange(ctx, groupID, linkID)
	if e != nil {
		return e
	}

	link.Status = domain.InviteStatusDecline

	if *link.CreatedBy != ctx.User.ID && (link.UserID == nil || *link.UserID != ctx.User.ID) {
		return goerr.New(app.i18n.Translatef(ctx.Locale,
			"its_arent_your_link")).HTTP(http.StatusForbidden)
	}

	if _, e = app.repository.UserLinkGroupAI.Save(link, nil); e != nil {
		return e
	}

	return nil
}

func (app *App) linkToChange(ctx Context, groupID int64,
	linkID int64) (link *domain.UserLinkGroup, e goerr.IError) {
	links, _, e := app.repository.UserLinkGroupAI.Search(&domain.UserLinkGroupSearchForm{
		SearchForm: domain.SearchForm{
			IDs: []int64{linkID},
		},
		GroupIDs: []int64{groupID},
		Statuses: []int{domain.InviteStatusNew},
	}, nil)

	if e != nil {
		return
	}

	if len(links) != 1 {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "links_not_found")).HTTP(http.StatusNotFound)

		return
	}

	return links[0], nil
}

func (app *App) InviteUser(ctx Context, groupID int64, form domain.InviteGroupForm) goerr.IError {
	entity, e := app.repository.GroupsAI.ByID(&domain.Groups{ID: groupID}, nil)
	if e != nil {
		return e
	}

	if e := app.canInviteGroup(ctx, ctx.User.ID, entity.ID, nil); e != nil {
		return e
	}

	us := app.cache.AuthAI.ByEmail(&domain.User{
		UserForm: domain.UserForm{Email: form.Email},
	})

	ulg := &domain.UserLinkGroup{}
	ulg.CreatedBy = &ctx.User.ID
	ulg.Roles = make([]int64, 0)
	ulg.Status = domain.InviteStatusNew
	ulg.GroupID = entity.ID
	ulg.CreatedBy = &ctx.User.ID
	ulg.Email = &form.Email

	if us != nil {
		ulg.UserID = &us.ID
	}

	_, p, e := app.repository.UserLinkGroupAI.Search(&domain.UserLinkGroupSearchForm{
		UserIDs:  nil,
		GroupIDs: []int64{entity.ID},
		Email:    form.Email,
		Statuses: []int{domain.InviteStatusNew, domain.InviteStatusAccept},
	}, nil)
	if e != nil {
		return e
	}

	if p.Total > 0 {
		return goerr.New(app.i18n.Translatef(ctx.Locale,
			"links_exists")).HTTP(http.StatusConflict)
	}

	link, e := app.createGroupLinks(ulg,
		nil)
	if e != nil {
		return e
	}

	if link != nil {
		msg := app.mailgun.NewMessage(
			MailSender,
			app.i18n.Translatef(ctx.Locale, "invite_in_group_subject"),
			app.i18n.Translatef(ctx.Locale, "invite_in_group_body"),
			form.Email,
		)

		if _, _, e = app.mailgun.Send(msg); e != nil {
			return e
		}
	}

	return nil
}

func (app *App) CreateGroups(ctx Context, groups *domain.Groups) (
	*domain.GroupsRender, goerr.IError) {
	groups.ID = 0

	if e := groups.Validate(); e != nil {
		return nil, e
	}

	tx, e := app.repository.Begin()
	if e != nil {
		return nil, e
	}

	newGroups, e := app.repository.GroupsAI.Save(groups, tx)
	if e != nil {
		return nil, e
	}

	ulg := &domain.UserLinkGroup{}
	ulg.UserID = &ctx.User.ID
	ulg.Roles = domain.GroupRoles()
	ulg.Status = domain.InviteStatusAccept
	ulg.GroupID = newGroups.ID
	ulg.CreatedBy = &ctx.User.ID

	link, e := app.createGroupLinks(ulg, tx)
	if e != nil {
		return nil, e
	}

	e = tx.Commit()
	if e != nil {
		return nil, e
	}

	groupsRender := app.prepareGroupsRender(ctx, *newGroups, []*domain.UserLinkGroupRender{
		app.prepareUserLinkGroupsRender(ctx, *link),
	})

	return groupsRender, nil
}

func (app *App) DeleteGroups(ctx Context, groups *domain.Groups) goerr.IError {
	entity, e := app.repository.GroupsAI.ByID(groups, nil)
	if e != nil {
		return e
	}

	if e := app.canChangeGroup(ctx, ctx.User.ID, entity.ID, nil); e != nil {
		return e
	}

	entity.DeletedAt = utils.TimePtr(time.Now())

	_, e = app.repository.GroupsAI.Save(entity, nil)
	if e != nil {
		return e
	}

	return nil
}

func (app *App) UpdateGroups(ctx Context, groups *domain.Groups) (
	*domain.GroupsRender, goerr.IError) {
	oldData, e := app.repository.GroupsAI.ByID(groups, nil)
	if e != nil {
		return nil, e
	}

	if e := app.canChangeGroup(ctx, ctx.User.ID, oldData.ID, nil); e != nil {
		return nil, e
	}

	oldData.Name = groups.Name

	if e := oldData.Validate(); e != nil {
		return nil, e
	}

	groupsEntity, e := app.repository.GroupsAI.Save(oldData, nil)
	if e != nil {
		return nil, e
	}

	links, _, e := app.repository.UserLinkGroupAI.Search(
		&domain.UserLinkGroupSearchForm{GroupIDs: []int64{groupsEntity.ID}}, nil)
	if e != nil {
		return nil, e
	}

	groupsRender := app.prepareGroupsRender(ctx, *groupsEntity,
		app.prepareUserLinkGroupsRenders(ctx, links))

	return groupsRender, nil
}

func (app *App) GetGroups(ctx Context, groups *domain.Groups) (
	*domain.GroupsRender, goerr.IError) {
	groupsEntity, e := app.repository.GroupsAI.ByID(groups, nil)
	if e != nil {
		return nil, e
	}

	if groupsEntity == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "groups_not_found")).HTTP(http.StatusNotFound)
	}

	if e := app.hasAnyRole(ctx, ctx.User.ID, groupsEntity.ID, map[int64]struct{}{}, nil); e != nil {
		return nil, e
	}

	links, _, e := app.repository.UserLinkGroupAI.Search(
		&domain.UserLinkGroupSearchForm{GroupIDs: []int64{groupsEntity.ID}}, nil)
	if e != nil {
		return nil, e
	}

	groupsRender := app.prepareGroupsRender(ctx, *groupsEntity,
		app.prepareUserLinkGroupsRenders(ctx, links))

	return groupsRender, nil
}

func (app *App) SearchGroups(ctx Context, searchGroups *domain.GroupsSearchForm) (
	[]*domain.GroupsRender, godict.Pagination, goerr.IError) {
	searchGroups.UserIDs = []int64{ctx.User.ID}

	groupss, pagination, e := app.repository.GroupsAI.Search(searchGroups, nil)
	if e != nil {
		return nil, pagination, e
	}

	renders := make([]*domain.GroupsRender, 0, len(groupss))

	grIDs := make([]int64, len(groupss))
	for i := range groupss {
		grIDs[i] = groupss[i].ID
	}

	links, _, e := app.repository.UserLinkGroupAI.Search(
		&domain.UserLinkGroupSearchForm{GroupIDs: grIDs}, nil)
	if e != nil {
		return nil, pagination, e
	}

	rendersLinks := app.prepareUserLinkGroupsRenders(ctx, links)

	for i := range groupss {
		renders = append(renders, app.prepareGroupsRender(ctx, *groupss[i], rendersLinks))
	}

	return renders, pagination, nil
}

func (app *App) prepareGroupsRender(ctx Context, groupsEntity domain.Groups,
	renders []*domain.UserLinkGroupRender) *domain.GroupsRender {
	_ = ctx

	rnd := make([]*domain.UserLinkGroupRender, 0)

	for i := range renders {
		if renders[i].GroupID == groupsEntity.ID {
			rnd = append(rnd, renders[i])
		}
	}

	return &domain.GroupsRender{
		Groups: groupsEntity,
		Users:  rnd,
	}
}

func (app *App) prepareUserLinkGroupsRenders(ctx Context,
	entities []*domain.UserLinkGroup) []*domain.UserLinkGroupRender {
	_ = ctx

	result := make([]*domain.UserLinkGroupRender, len(entities))

	for i := range entities {
		result[i] = &domain.UserLinkGroupRender{
			UserLinkGroup: *entities[i],
		}

		if entities[i].UserID != nil && app.cache.AuthAI.ByID(*entities[i].UserID) != nil {
			result[i].User = app.PrepareUserRender(ctx,
				*app.cache.AuthAI.ByID(*entities[i].UserID))
		}
	}

	return result
}

func (app *App) prepareUserLinkGroupsRender(ctx Context,
	entity domain.UserLinkGroup) *domain.UserLinkGroupRender {
	_ = ctx

	result := &domain.UserLinkGroupRender{
		UserLinkGroup: entity,
	}

	if result.UserID != nil && app.cache.AuthAI.ByID(*result.UserID) != nil {
		result.User = app.PrepareUserRender(ctx,
			*app.cache.AuthAI.ByID(*result.UserID))
	}

	return result
}

func (app *App) AddRole(ctx Context,
	groupID int64, userID int64, roles *domain.ChangeRoleForm) goerr.IError {
	e := app.canRightGroup(ctx, ctx.User.ID, groupID, nil)
	if e != nil {
		return e
	}

	links, _, e := app.repository.UserLinkGroupAI.Search(
		&domain.UserLinkGroupSearchForm{
			GroupIDs: []int64{groupID},
			UserIDs:  []int64{userID},
			Statuses: []int{domain.InviteStatusAccept},
		}, nil)
	if e != nil {
		return e
	}

	if len(links) == 0 {
		return goerr.New("link_not_found").HTTP(http.StatusNotFound)
	}

	if e := domain.ValidateGroupRoles(roles.Roles); e != nil {
		return goerr.New(app.i18n.Translatef(ctx.Locale,
			"role_conflict")).HTTP(http.StatusConflict)
	}

	link := links[0]
	rl := link.Roles
	mp := make(map[int]struct{})

	for i := range rl {
		mp[int(rl[i])] = struct{}{}
	}

	var changed bool

	for i := range roles.Roles {
		if _, ok := mp[roles.Roles[i]]; !ok {
			rl = append(rl, int64(roles.Roles[i]))

			changed = true
		}
	}

	if changed {
		link.Roles = rl
		if _, e := app.repository.UserLinkGroupAI.Save(link, nil); e != nil {
			return nil
		}
	}

	return nil
}

func (app *App) DropRole(ctx Context,
	groupID int64, userID int64, roles *domain.ChangeRoleForm) goerr.IError {
	e := app.canRightGroup(ctx, ctx.User.ID, groupID, nil)
	if e != nil {
		return e
	}

	links, _, e := app.repository.UserLinkGroupAI.Search(
		&domain.UserLinkGroupSearchForm{
			GroupIDs: []int64{groupID},
			UserIDs:  []int64{userID},
			Statuses: []int{domain.InviteStatusAccept},
		}, nil)
	if e != nil {
		return e
	}

	if len(links) == 0 {
		return goerr.New("link_not_found").HTTP(http.StatusNotFound)
	}

	if e := domain.ValidateGroupRoles(roles.Roles); e != nil {
		return goerr.New(app.i18n.Translatef(ctx.Locale,
			"role_conflict")).HTTP(http.StatusConflict)
	}

	link := links[0]
	rl := make([]int64, 0)
	mp := make(map[int]struct{})

	for i := range roles.Roles {
		mp[roles.Roles[i]] = struct{}{}
	}

	var changed bool

	for i := range link.Roles {
		if _, ok := mp[int(link.Roles[i])]; !ok {
			rl = append(rl, link.Roles[i])

			changed = true
		}
	}

	if changed {
		link.Roles = rl

		if e = app.hasAdminUser(ctx, link.GroupID, link); e != nil {
			return e
		}

		if _, e := app.repository.UserLinkGroupAI.Save(link, nil); e != nil {
			return nil
		}
	}

	return nil
}

func (app *App) hasAdminUser(ctx Context, groupID int64, link *domain.UserLinkGroup) goerr.IError {
	for i := range link.Roles {
		if link.Roles[i] == domain.GroupRoleRights {
			return nil
		}
	}

	form := &domain.UserLinkGroupSearchForm{
		GroupIDs:   []int64{groupID},
		Statuses:   []int{domain.InviteStatusAccept},
		SearchForm: domain.SearchForm{},
	}

	if link != nil {
		form.ExcludedIDs = []int64{link.ID}
	}

	links, _, e := app.repository.UserLinkGroupAI.Search(form, nil)
	if e != nil {
		return e
	}

	for i := range links {
		for j := range links[i].Roles {
			if links[i].Roles[j] == domain.GroupRoleRights {
				return nil
			}
		}
	}

	return goerr.New(app.i18n.Translatef(ctx.Locale, "last_admint_delete")).HTTP(http.StatusConflict)
}
