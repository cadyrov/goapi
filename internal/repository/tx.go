package repository

import (
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
)

type realTransaction struct {
	*gopsql.Tx
	unique bool
}

func (realTransaction *realTransaction) Commit() goerr.IError {
	if realTransaction.unique {
		return nil
	}

	return realTransaction.commitRp()
}

func (realTransaction *realTransaction) rollbackRp() goerr.IError {
	e := realTransaction.Tx.Rollback()

	if e != nil {
		return goerr.New(e.Error())
	}

	return nil
}

func (realTransaction *realTransaction) commitRp() goerr.IError {
	e := realTransaction.Tx.Commit()
	if e != nil {
		return goerr.New(e.Error())
	}

	return nil
}

func (realTransaction *realTransaction) Rollback() goerr.IError {
	if realTransaction.unique {
		return nil
	}

	return realTransaction.rollbackRp()
}

func (realTransaction *realTransaction) Querier() gopsql.Queryer {
	return realTransaction.Tx
}
