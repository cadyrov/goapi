package access

import (
	"goapi/internal/domain"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
)

type Transacter interface {
	Commit() goerr.IError
	Rollback() goerr.IError
	Querier() gopsql.Queryer
}

type UserAI interface {
	Save(user *domain.User, tx Transacter) (*domain.User, goerr.IError)
	Delete(user *domain.User, tx Transacter) goerr.IError
	ByID(user *domain.User, tx Transacter) (*domain.User, goerr.IError)
	ByHash(user *domain.User, tx Transacter) (*domain.User, goerr.IError)
	Search(userSearchForm *domain.UserSearchForm,
		tx Transacter) ([]*domain.User, godict.Pagination, goerr.IError)
}

type GroupsAI interface {
	Save(groups *domain.Groups, tx Transacter) (*domain.Groups, goerr.IError)
	Delete(groups *domain.Groups, tx Transacter) goerr.IError
	ByID(groups *domain.Groups, tx Transacter) (*domain.Groups, goerr.IError)
	Search(groupsSearchForm *domain.GroupsSearchForm,
		tx Transacter) ([]*domain.Groups, godict.Pagination, goerr.IError)
}

type UserLinkGroupAI interface {
	Save(userLinkGroup *domain.UserLinkGroup,
		tx Transacter) (*domain.UserLinkGroup, goerr.IError)
	Delete(userLinkGroup *domain.UserLinkGroup, tx Transacter) goerr.IError
	ByID(userLinkGroup *domain.UserLinkGroup,
		tx Transacter) (*domain.UserLinkGroup, goerr.IError)
	Search(userLinkGroup *domain.UserLinkGroupSearchForm, tx Transacter) (
		[]*domain.UserLinkGroup, godict.Pagination, goerr.IError)
}
