package repository

import (
	"goapi/internal/repository/access"
	"goapi/internal/repository/psql"
	"goapi/tools/logger"

	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
)

type Repository struct {
	logger          logger.Logger
	PSQLQueryer     gopsql.Queryer
	UserAI          access.UserAI
	GroupsAI        access.GroupsAI
	UserLinkGroupAI access.UserLinkGroupAI
}

type Config struct {
	DB gopsql.Config `yaml:"psql"`
}

func New(config Config, logger logger.Logger, isTransactional bool) (*Repository, goerr.IError) {
	r := Repository{logger: logger}

	DB, e := config.DB.Connect()
	if e != nil {
		return nil, e
	}

	r.PSQLQueryer = DB

	if isTransactional {
		r.PSQLQueryer, e = DB.Begin()
		if e != nil {
			return nil, e
		}
	}

	r.plSQLCreate()

	return &r, nil
}

func (r *Repository) plSQLCreate() {
	r.UserAI = psql.NewUser(r.PSQLQueryer)
	r.GroupsAI = psql.NewGroups(r.PSQLQueryer)
	r.UserLinkGroupAI = psql.NewUserLinkGroup(r.PSQLQueryer)
}

func (r *Repository) Begin() (access.Transacter, goerr.IError) {
	rt := realTransaction{}

	if r.isTransactional() {
		rt.Tx = r.PSQLQueryer.(*gopsql.Tx)
		rt.unique = true
	} else {
		if r.PSQLQueryer == nil {
			return nil, nil
		}

		txPSQL, e := r.PSQLQueryer.(*gopsql.DB).Begin()
		if e != nil {
			return nil, e
		}

		rt.Tx = txPSQL
	}

	return &rt, nil
}

func (r *Repository) isTransactional() bool {
	_, ok := r.PSQLQueryer.(*gopsql.Tx)

	return ok
}
