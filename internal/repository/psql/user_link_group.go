package psql

import (
	"database/sql"
	"goapi/internal/domain"
	"goapi/internal/repository/access"
	"goapi/utils"
	"strings"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
	"github.com/lib/pq"
)

type UserLinkGroupDAO struct {
	gopsql.Queryer
}

func NewUserLinkGroup(db gopsql.Queryer) *UserLinkGroupDAO {
	return &UserLinkGroupDAO{Queryer: db}
}

func (userLinkGroupDAO *UserLinkGroupDAO) Table() string {
	return "public.user_link_group"
}

func (userLinkGroupDAO *UserLinkGroupDAO) Columns() []string {
	return []string{
		"id", "user_id", "group_id", "roles", "created_at", "created_by",
		"updated_at", "updated_by", "deleted_at", "deleted_by", "status", "email",
	}
}

func (userLinkGroupDAO *UserLinkGroupDAO) Values(d *domain.UserLinkGroup) (values []interface{}) {
	return append(values, &d.ID, &d.UserID, &d.GroupID, pq.Array(&d.Roles), &d.CreatedAt, &d.CreatedBy,
		&d.UpdatedAt, &d.UpdatedBy, &d.DeletedAt, &d.DeletedBy, &d.Status, &d.Email)
}

func (userLinkGroupDAO *UserLinkGroupDAO) ByID(d *domain.UserLinkGroup, tx access.Transacter) (*domain.UserLinkGroup,
	goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(userLinkGroupDAO.Columns(), ","))

	queryBuilder.Add("FROM " + userLinkGroupDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = userLinkGroupDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	userLinkGroupEntity, e := userLinkGroupDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	if userLinkGroupEntity == nil {
		return nil, e
	}

	return userLinkGroupEntity, nil
}

func (userLinkGroupDAO *UserLinkGroupDAO) Delete(d *domain.UserLinkGroup, tx access.Transacter) goerr.IError {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Add("DELETE FROM " + userLinkGroupDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = userLinkGroupDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	if _, e := queryer.Exec(queryBuilder.RawSQL(), queryBuilder.Values()...); e != nil {
		return e
	}

	return nil
}

func (userLinkGroupDAO *UserLinkGroupDAO) Save(d *domain.UserLinkGroup, tx access.Transacter) (
	*domain.UserLinkGroup, goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	if d.ID != 0 {
		queryBuilder.Add("UPDATE " + userLinkGroupDAO.Table())
		queryBuilder.Add("SET  user_id = ? , group_id = ? , roles = ? , created_at = ? , created_by = ? , "+
			"updated_at = ? , updated_by = ? , deleted_at = ? , deleted_by = ?, status = ?, email = ?",
			&d.UserID, &d.GroupID, pq.Array(&d.Roles), &d.CreatedAt, &d.CreatedBy,
			&d.UpdatedAt, &d.UpdatedBy, &d.DeletedAt, &d.DeletedBy, &d.Status, &d.Email)
		queryBuilder.Add("WHERE id = ?", d.ID)
	} else {
		queryBuilder.Add("INSERT INTO " + userLinkGroupDAO.Table())
		queryBuilder.Add("( user_id , group_id , roles , created_at , created_by , updated_at , "+
			"updated_by , deleted_at , deleted_by, status , email ) "+
			"VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ?, ?, ?)",
			&d.UserID, &d.GroupID, pq.Array(&d.Roles), &d.CreatedAt, &d.CreatedBy,
			&d.UpdatedAt, &d.UpdatedBy, &d.DeletedAt, &d.DeletedBy, &d.Status, &d.Email)
	}

	queryBuilder.Add("RETURNING " + strings.Join(userLinkGroupDAO.Columns(), ","))

	var queryer gopsql.Queryer
	queryer = userLinkGroupDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	userLinkGroupEntity, e := userLinkGroupDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	return userLinkGroupEntity, nil
}

func (userLinkGroupDAO *UserLinkGroupDAO) Search(
	userLinkGroupSearchForm *domain.UserLinkGroupSearchForm, tx access.Transacter) (
	userLinkGroups []*domain.UserLinkGroup, pagination godict.Pagination, e goerr.IError) {
	queryBuilder := userLinkGroupDAO.prepareSearchBuilder(userLinkGroupSearchForm)

	countErrChan := make(chan utils.CounterErr, 1)

	queryer := userLinkGroupDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	_, isTransaction := queryer.(*gopsql.Tx)

	if !isTransaction {
		utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	} else {
		go utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	}

	userLinkGroups, e = userLinkGroupDAO.Collection(queryer.Query(queryBuilder.RawSQL(),
		queryBuilder.Values()...))
	if e != nil {
		return userLinkGroups, pagination, e
	}

	ch := <-countErrChan
	if ch.Err != nil {
		return userLinkGroups, pagination, ch.Err
	}

	pagination.Total = ch.Total

	return userLinkGroups, pagination, nil
}

func (userLinkGroupDAO *UserLinkGroupDAO) prepareSearchBuilder(
	userLinkGroupSearchForm *domain.UserLinkGroupSearchForm) *gopsql.Builder {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(userLinkGroupDAO.Columns(), ","))
	queryBuilder.Add(" FROM " + userLinkGroupDAO.Table() + " WHERE 1 = 1")

	if len(userLinkGroupSearchForm.UserIDs) != 0 {
		queryBuilder.Add("AND user_id = ANY(?)",
			pq.Array(&userLinkGroupSearchForm.UserIDs))
	}

	if len(userLinkGroupSearchForm.GroupIDs) != 0 {
		queryBuilder.Add("AND group_id = ANY(?)",
			pq.Array(&userLinkGroupSearchForm.GroupIDs))
	}

	if len(userLinkGroupSearchForm.IDs) > 0 {
		queryBuilder.Add("AND id = ANY(?)",
			pq.Array(&userLinkGroupSearchForm.IDs))
	}

	if len(userLinkGroupSearchForm.ExcludedIDs) > 0 {
		queryBuilder.Add("AND NOT (id = ANY(?))",
			pq.Array(&userLinkGroupSearchForm.ExcludedIDs))
	}

	if len(userLinkGroupSearchForm.Statuses) != 0 {
		queryBuilder.Add("AND status = ANY(?)",
			pq.Array(userLinkGroupSearchForm.Statuses))
	}

	if userLinkGroupSearchForm.Email != "" {
		queryBuilder.Add("AND email = ?",
			userLinkGroupSearchForm.Email)
	}

	queryBuilder.Pagination(userLinkGroupSearchForm.Limit, userLinkGroupSearchForm.Page-1)

	return queryBuilder
}

func (userLinkGroupDAO *UserLinkGroupDAO) Collection(
	rows *sql.Rows, err goerr.IError) (collection []*domain.UserLinkGroup,
	e goerr.IError) {
	collection = make([]*domain.UserLinkGroup, 0)

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		e = err

		return
	}

	for rows.Next() {
		userLinkGroup := domain.UserLinkGroup{}

		err := rows.Scan(userLinkGroupDAO.Values(&userLinkGroup)...)
		if err != nil {
			e = goerr.New(err.Error())

			return
		}

		collection = append(collection, &userLinkGroup)
	}

	return collection, nil
}

func (userLinkGroupDAO *UserLinkGroupDAO) One(
	rows *sql.Rows, err goerr.IError) (userLinkGroup *domain.UserLinkGroup, e goerr.IError) {
	userLinkGroups, e := userLinkGroupDAO.Collection(rows, err)
	if e != nil {
		return
	}

	if len(userLinkGroups) == 0 {
		return
	}

	return userLinkGroups[0], nil
}
