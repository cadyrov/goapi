package psql

import (
	"database/sql"
	"goapi/internal/domain"
	"goapi/internal/repository/access"
	"goapi/utils"
	"strings"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
	"github.com/lib/pq"
)

type GroupsDAO struct {
	gopsql.Queryer
}

func NewGroups(db gopsql.Queryer) *GroupsDAO {
	return &GroupsDAO{Queryer: db}
}

func (groupsDAO *GroupsDAO) Table() string {
	return "public.groups"
}

func (groupsDAO *GroupsDAO) Columns() []string {
	return []string{"id", "name", "deleted_at"}
}

func (groupsDAO *GroupsDAO) Values(d *domain.Groups) (values []interface{}) {
	return append(values, &d.ID, &d.Name, &d.DeletedAt)
}

func (groupsDAO *GroupsDAO) ByID(d *domain.Groups, tx access.Transacter) (*domain.Groups,
	goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(groupsDAO.Columns(), ","))

	queryBuilder.Add("FROM " + groupsDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = groupsDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	groupsEntity, e := groupsDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	if groupsEntity == nil {
		return nil, e
	}

	return groupsEntity, nil
}

func (groupsDAO *GroupsDAO) Delete(d *domain.Groups, tx access.Transacter) goerr.IError {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Add("DELETE FROM " + groupsDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = groupsDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	if _, e := queryer.Exec(queryBuilder.RawSQL(), queryBuilder.Values()...); e != nil {
		return e
	}

	return nil
}

func (groupsDAO *GroupsDAO) Save(d *domain.Groups, tx access.Transacter) (
	*domain.Groups, goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	if d.ID != 0 {
		queryBuilder.Add("UPDATE " + groupsDAO.Table())
		queryBuilder.Add("SET  name = ? , deleted_at = ?", &d.Name, &d.DeletedAt)
		queryBuilder.Add("WHERE id = ?", d.ID)
	} else {
		queryBuilder.Add("INSERT INTO " + groupsDAO.Table())
		queryBuilder.Add("( name , deleted_at) VALUES ( ? , ?)", &d.Name, &d.DeletedAt)
	}

	queryBuilder.Add("RETURNING " + strings.Join(groupsDAO.Columns(), ","))

	var queryer gopsql.Queryer
	queryer = groupsDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	groupsEntity, e := groupsDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	return groupsEntity, nil
}

func (groupsDAO *GroupsDAO) Search(groupsSearchForm *domain.GroupsSearchForm, tx access.Transacter) (
	groupss []*domain.Groups, pagination godict.Pagination, e goerr.IError) {
	queryBuilder := groupsDAO.prepareSearchBuilder(groupsSearchForm)

	countErrChan := make(chan utils.CounterErr, 1)

	queryer := groupsDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	_, isTransaction := queryer.(*gopsql.Tx)

	if !isTransaction {
		utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	} else {
		go utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	}

	groupss, e = groupsDAO.Collection(queryer.Query(queryBuilder.RawSQL(),
		queryBuilder.Values()...))
	if e != nil {
		return groupss, pagination, e
	}

	ch := <-countErrChan
	if ch.Err != nil {
		return groupss, pagination, ch.Err
	}

	pagination.Total = ch.Total

	return groupss, pagination, nil
}

func (groupsDAO *GroupsDAO) prepareSearchBuilder(
	groupsSearchForm *domain.GroupsSearchForm) *gopsql.Builder {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(groupsDAO.Columns(), ","))
	queryBuilder.Add(" FROM " + groupsDAO.Table() + " WHERE 1 = 1")

	if groupsSearchForm.Query != "" {
		queryBuilder.Add("AND name LIKE ?",
			utils.ToLike(&groupsSearchForm.Query))
	}

	if len(groupsSearchForm.UserIDs) > 0 {
		queryBuilder.Add(`AND EXISTS  
				(SELECT 1 
				FROM  public.user_link_group ulg 
				WHERE ulg.group_id = groups.id
				AND ulg.user_id = ANY(?)
			)`,
			pq.Array(&groupsSearchForm.UserIDs))
	}

	if len(groupsSearchForm.IDs) > 0 {
		queryBuilder.Add("AND id = ANY(?)",
			pq.Array(&groupsSearchForm.IDs))
	}

	if len(groupsSearchForm.ExcludedIDs) > 0 {
		queryBuilder.Add("AND NOT (id = ANY(?))",
			pq.Array(&groupsSearchForm.ExcludedIDs))
	}

	queryBuilder.Pagination(groupsSearchForm.Limit, groupsSearchForm.Page-1)

	return queryBuilder
}

func (groupsDAO *GroupsDAO) Collection(rows *sql.Rows, err goerr.IError) (collection []*domain.Groups,
	e goerr.IError) {
	collection = make([]*domain.Groups, 0)

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		e = err

		return
	}

	for rows.Next() {
		groups := domain.Groups{}

		err := rows.Scan(groupsDAO.Values(&groups)...)
		if err != nil {
			e = goerr.New(err.Error())

			return
		}

		collection = append(collection, &groups)
	}

	return collection, nil
}

func (groupsDAO *GroupsDAO) One(rows *sql.Rows, err goerr.IError) (groups *domain.Groups, e goerr.IError) {
	groupss, e := groupsDAO.Collection(rows, err)
	if e != nil {
		return
	}

	if len(groupss) == 0 {
		return
	}

	return groupss[0], nil
}
