package psql

import (
	"database/sql"
	"goapi/internal/domain"
	"goapi/internal/repository/access"
	"goapi/utils"
	"strings"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
	"github.com/lib/pq"
)

type UserDAO struct {
	gopsql.Queryer
}

func NewUser(db gopsql.Queryer) *UserDAO {
	return &UserDAO{Queryer: db}
}

func (userDAO *UserDAO) Table() string {
	return "public.user"
}

func (userDAO *UserDAO) Columns() []string {
	return []string{"id", "username", "password_hash", "email", "roles", "status_id"}
}

func (userDAO *UserDAO) Values(d *domain.User) (values []interface{}) {
	return append(values, &d.ID, &d.Username, &d.PasswordHash, &d.Email, pq.Array(&d.Roles), &d.StatusID)
}

func (userDAO *UserDAO) ByID(d *domain.User, tx access.Transacter) (*domain.User,
	goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(userDAO.Columns(), ","))

	queryBuilder.Add("FROM " + userDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = userDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	userEntity, e := userDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	if userEntity == nil {
		return nil, e
	}

	return userEntity, nil
}

func (userDAO *UserDAO) ByHash(d *domain.User, tx access.Transacter) (*domain.User,
	goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(userDAO.Columns(), ","))

	queryBuilder.Add("FROM " + userDAO.Table())

	queryBuilder.Add("WHERE password_hash = ?", d.PasswordHash)

	var queryer gopsql.Queryer
	queryer = userDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	userEntity, e := userDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	return userEntity, nil
}

func (userDAO *UserDAO) Delete(d *domain.User, tx access.Transacter) goerr.IError {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Add("DELETE FROM " + userDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = userDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	if _, e := queryer.Exec(queryBuilder.RawSQL(), queryBuilder.Values()...); e != nil {
		return e
	}

	return nil
}

func (userDAO *UserDAO) Save(d *domain.User, tx access.Transacter) (
	*domain.User, goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	if d.ID != 0 {
		queryBuilder.Add("UPDATE " + userDAO.Table())
		queryBuilder.Add("SET  username = ? , password_hash = ? "+
			", email = ? , roles = ? , status_id = ?", &d.Username,
			&d.PasswordHash, &d.Email, pq.Array(&d.Roles), &d.StatusID)
		queryBuilder.Add("WHERE id = ?", d.ID)
	} else {
		queryBuilder.Add("INSERT INTO " + userDAO.Table())
		queryBuilder.Add("( username , password_hash , "+
			"email , roles , status_id) VALUES ( ? , ? , ? , ? , ?)", &d.Username,
			&d.PasswordHash, &d.Email, pq.Array(&d.Roles), &d.StatusID)
	}

	queryBuilder.Add("RETURNING " + strings.Join(userDAO.Columns(), ","))

	var queryer gopsql.Queryer
	queryer = userDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	userEntity, e := userDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	return userEntity, nil
}

func (userDAO *UserDAO) Search(userSearchForm *domain.UserSearchForm, tx access.Transacter) (
	users []*domain.User, pagination godict.Pagination, e goerr.IError) {
	queryBuilder := userDAO.prepareSearchBuilder(userSearchForm)

	countErrChan := make(chan utils.CounterErr, 1)

	queryer := userDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	_, isTransaction := queryer.(*gopsql.Tx)

	if !isTransaction {
		utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	} else {
		go utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	}

	users, e = userDAO.Collection(queryer.Query(queryBuilder.RawSQL(),
		queryBuilder.Values()...))
	if e != nil {
		return users, pagination, e
	}

	ch := <-countErrChan
	if ch.Err != nil {
		return users, pagination, ch.Err
	}

	pagination.Total = ch.Total

	return users, pagination, nil
}

func (userDAO *UserDAO) prepareSearchBuilder(
	userSearchForm *domain.UserSearchForm) *gopsql.Builder {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(userDAO.Columns(), ","))
	queryBuilder.Add(" FROM " + userDAO.Table() + " WHERE 1 = 1")

	if userSearchForm.Query != "" {
		queryBuilder.Add("AND name LIKE ?",
			utils.ToLike(&userSearchForm.Query))
	}

	if len(userSearchForm.IDs) > 0 {
		queryBuilder.Add("AND id = ANY(?)",
			pq.Array(&userSearchForm.IDs))
	}

	if len(userSearchForm.ExcludedIDs) > 0 {
		queryBuilder.Add("AND NOT (id = ANY(?))",
			pq.Array(&userSearchForm.ExcludedIDs))
	}

	if userSearchForm.Email != "" {
		queryBuilder.Add("AND email LIKE ?",
			&userSearchForm.Email)
	}

	queryBuilder.Pagination(userSearchForm.Limit, userSearchForm.Page-1)

	return queryBuilder
}

func (userDAO *UserDAO) Collection(rows *sql.Rows, err goerr.IError) (collection []*domain.User,
	e goerr.IError) {
	collection = make([]*domain.User, 0)

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		e = err

		return
	}

	for rows.Next() {
		user := domain.User{}

		err := rows.Scan(userDAO.Values(&user)...)
		if err != nil {
			e = goerr.New(err.Error())

			return
		}

		collection = append(collection, &user)
	}

	return collection, nil
}

func (userDAO *UserDAO) One(rows *sql.Rows, err goerr.IError) (user *domain.User, e goerr.IError) {
	users, e := userDAO.Collection(rows, err)
	if e != nil {
		return
	}

	if len(users) == 0 {
		return
	}

	return users[0], nil
}
