package domain

import (
	"net/http"

	"github.com/cadyrov/goerr"
)

type SearchForm struct {
	Query       string  `json:"query"`
	IDs         []int64 `json:"ids"`
	ExcludedIDs []int64 `json:"excludedIds"`
	Limit       int     `json:"limit"`
	Page        int     `json:"page"`
}

const (
	InConstant = 10
	InInvest   = 20

	OutConstant = 110
	OutVariable = 120
	OutInvest   = 150
	OutTax      = 190
)

const (
	GroupRoleRights     = 10
	GroupRoleInvite     = 20
	GroupRoleChange     = 30
	GroupRoleDictionary = 40
)

func HardSatates() []int64 {
	return []int64{InConstant, InInvest, OutConstant, OutVariable, OutInvest, OutTax}
}

func ValidateHardStates(hard []int64) goerr.IError {
	for i := range hard {
		exists := true

		for j := range HardSatates() {
			if HardSatates()[j] == hard[i] {
				exists = true
			}
		}

		if !exists {
			return goerr.New("hard_states").HTTP(http.StatusConflict)
		}
	}

	return nil
}

func GroupRoles() []int64 {
	return []int64{GroupRoleRights, GroupRoleInvite, GroupRoleChange, GroupRoleDictionary}
}

func ValidateGroupRoles(roles []int) goerr.IError {
	for i := range roles {
		switch roles[i] {
		case GroupRoleRights:
			return nil
		case GroupRoleInvite:
			return nil
		case GroupRoleChange:
			return nil
		case GroupRoleDictionary:
			return nil
		default:
			return goerr.New("roles").HTTP(http.StatusConflict)
		}
	}

	return nil
}
