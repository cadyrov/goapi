package domain

import (
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
)

type UserStatus int

const (
	UserStatusNew    UserStatus = 10
	UserStatusActive UserStatus = 20
	UserStatusBlock  UserStatus = 30
)

// User domain.
type User struct {
	ID           int64      `column:"id" json:"id"`
	PasswordHash *string    `column:"password_hash" json:"-"`
	Roles        []string   `column:"roles" json:"roles"`        // Роли пользования
	StatusID     UserStatus `column:"status_id" json:"statusId"` // Статус пользователя
	UserForm
}

type UserForm struct {
	Username string `column:"username" json:"username"` // Имя
	Email    string `column:"email" json:"email"`       // Эл. почта
}

type UserRender struct {
	User
	Status godict.DictionaryRender `json:"status"`
}

type UserSearchForm struct {
	SearchForm
	Email string
}

func (m *User) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Username, validation.Required),
		validation.Field(&m.StatusID, validation.Required),
		validation.Field(&m.Email, validation.Required),
	)
}
