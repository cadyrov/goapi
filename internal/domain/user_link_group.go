package domain

import (
	"time"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
)

const (
	InviteStatusNew     = 10
	InviteStatusAccept  = 20
	InviteStatusDecline = 30
)

// UserLinkGroup domain.
type UserLinkGroup struct {
	UserLinkGroupForm
}

type InviteGroupForm struct {
	Email string `json:"email"`
}

type ChangeRoleForm struct {
	Roles []int `json:"roles"`
}

type UserLinkGroupForm struct {
	ID        int64      `column:"id" json:"id"`                // Id записи
	UserID    *int64     `column:"user_id" json:"userId"`       // Id пользователя
	GroupID   int64      `column:"group_id" json:"groupId"`     // Id группы
	Roles     []int64    `column:"roles" json:"roles"`          // Id ролей
	CreatedAt time.Time  `column:"created_at" json:"createdAt"` // Создан
	CreatedBy *int64     `column:"created_by" json:"createdBy"` // Кем создан
	UpdatedAt *time.Time `column:"updated_at" json:"updatedAt"` // Изменен
	UpdatedBy *int64     `column:"updated_by" json:"updatedBy"` // Кем изменен
	DeletedAt *time.Time `column:"deleted_at" json:"deletedAt"` // Отмечен удаленным
	DeletedBy *int64     `column:"deleted_by" json:"deletedBy"` // Кем удален
	Status    int        `column:"status" json:"status"`        // Статус события
	Email     *string    `column:"email" json:"email"`          // email приглашенного
}

type UserLinkGroupRender struct {
	UserLinkGroup
	User *UserRender `json:"user"`
}

type UserLinkGroupSearchForm struct {
	SearchForm
	UserIDs  []int64
	GroupIDs []int64
	Email    string
	Statuses []int
}

func (m *UserLinkGroup) Validate() (e goerr.IError) {
	e = validation.ValidateStruct(m,
		validation.Field(&m.GroupID, validation.Required),
		validation.Field(&m.CreatedAt, validation.Required),
		validation.Field(&m.Status, validation.Required),
	)

	if e != nil {
		return
	}

	if m.Email == nil && m.UserID == nil {
		return goerr.New("send userID or email")
	}

	return nil
}
