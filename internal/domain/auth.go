package domain

import (
	"net/http"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
)

type LoginForm struct {
	Password string `json:"password"`
	Login    string `json:"login"`
}

func (lf *LoginForm) Validate() (e goerr.IError) {
	return validation.ValidateStruct(lf,
		validation.Field(&lf.Login, validation.Required),
		validation.Field(&lf.Password, validation.Required),
	)
}

type ValidationKey string

type ChangePasswordForm struct {
	UserID   int64         `json:"userId"`
	Password string        `json:"password"`
	Confirm  string        `json:"confirm"`
	Hash     ValidationKey `json:"hash"`
}

func (changePasswordForm *ChangePasswordForm) Validate(hash bool) (err goerr.IError) {
	if hash && changePasswordForm.Hash == "" {
		err = goerr.New("Hash is empty").HTTP(http.StatusBadRequest)

		return
	}

	if changePasswordForm.Password == "" {
		err = goerr.New("Password must not be empty").HTTP(http.StatusBadRequest)

		return
	}

	if changePasswordForm.Confirm == "" {
		err = goerr.New("ConfirmPassword must not be empty").HTTP(http.StatusBadRequest)

		return
	}

	if changePasswordForm.Password != changePasswordForm.Confirm {
		err = goerr.New("Password not equal ConfirmPassword").HTTP(http.StatusBadRequest)

		return
	}

	return err
}
