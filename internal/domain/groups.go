package domain

import (
	"time"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
)

// Groups domain.
type Groups struct {
	ID        int64      `column:"id" json:"id"`                // Ид группы
	DeletedAt *time.Time `column:"deleted_at" json:"deletedAt"` // Дата удаления
	GroupsForm
}

type GroupsForm struct {
	Name string `column:"name" json:"name"` // Название
}

type GroupsRender struct {
	Groups
	Users []*UserLinkGroupRender `json:"users"`
}

type GroupsSearchForm struct {
	UserIDs []int64 `json:"userIds"`
	SearchForm
}

func (m *Groups) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Name, validation.Required),
	)
}
