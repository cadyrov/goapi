package cache

import (
	"goapi/internal/cache/access"
	"goapi/internal/cache/memory"
	"goapi/tools/logger"

	"github.com/cadyrov/goerr"
)

type Cache struct {
	logger logger.Logger
	AuthAI access.AuthAI
}

func New(logger logger.Logger) (*Cache, goerr.IError) {
	r := Cache{logger: logger}

	r.memoryCreate()

	return &r, nil
}

func (r *Cache) memoryCreate() {
	storage := memory.NewStorage()

	r.AuthAI = storage
}
