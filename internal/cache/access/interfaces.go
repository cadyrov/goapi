package access

import (
	"goapi/internal/domain"
)

type AuthAI interface {
	SetUsers(list []*domain.User)
	ByEmail(user *domain.User) *domain.User
	ByID(id int64) *domain.User
	AddOnKey(key domain.ValidationKey, userID int64)
	ByValidationKey(key domain.ValidationKey) *domain.User
	DropValidationKey(key domain.ValidationKey)
	Set(user *domain.User)
	Drop(user *domain.User)
}
