package memory

import (
	"goapi/internal/domain"
	"sync"
)

type Storage struct {
	mx    sync.RWMutex
	users []*domain.User
	auth  map[string]int64
}

func NewStorage() *Storage {
	return &Storage{auth: make(map[string]int64)}
}

func (s *Storage) SetUsers(list []*domain.User) {
	s.mx.Lock()

	defer s.mx.Unlock()

	s.users = list
}

func (s *Storage) ByEmail(user *domain.User) *domain.User {
	s.mx.RLock()

	defer s.mx.RUnlock()

	for i := range s.users {
		if s.users[i].Email == user.Email {
			return s.users[i]
		}
	}

	return nil
}

func (s *Storage) ByID(id int64) *domain.User {
	s.mx.RLock()

	defer s.mx.RUnlock()

	for i := range s.users {
		if s.users[i].ID == id {
			return s.users[i]
		}
	}

	return nil
}

func (s *Storage) ByValidationKey(key domain.ValidationKey) *domain.User {
	ID, ok := s.auth[string(key)]
	if !ok {
		return nil
	}

	s.mx.Lock()

	defer s.mx.Unlock()

	for i := range s.users {
		if s.users[i].ID == ID {
			return s.users[i]
		}
	}

	return nil
}

func (s *Storage) DropValidationKey(key domain.ValidationKey) {
	s.mx.Lock()

	defer s.mx.Unlock()

	delete(s.auth, string(key))
}

func (s *Storage) AddOnKey(key domain.ValidationKey, userID int64) {
	s.mx.Lock()

	defer s.mx.Unlock()

	s.auth[string(key)] = userID
}

func (s *Storage) Set(user *domain.User) {
	if user == nil {
		return
	}

	s.mx.Lock()

	defer s.mx.Unlock()

	for i := range s.users {
		if s.users[i].ID == user.ID {
			s.users[i] = user

			return
		}
	}

	s.users = append(s.users, user)
}

func (s *Storage) Drop(user *domain.User) {
	if user == nil {
		return
	}

	s.mx.Lock()

	defer s.mx.Unlock()

	newUsers := make([]*domain.User, 0, len(s.users))

	for i := range s.users {
		if s.users[i].ID == user.ID {
			continue
		}

		newUsers = append(newUsers, s.users[i])
	}

	s.users = newUsers
}
