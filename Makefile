TAG=$(shell git branch | sed -n -e 's/\* //p')
PROJECT = $(shell basename `pwd`)
REGISTRY = goapi

prune:
	docker container prune
	docker image prune

build:
	go build -o ./bin/$(PROJECT) ./cmd/http/main.go
	go build -o ./bin/translator ./cmd/translate/main.go
	go build -o ./bin/migrate ./cmd/migrate/main.go
	go build -o ./bin/codegen ./cmd/codegen/main.go

help:
	make build
	env ENV=local ./$(PROJECT) h

swaggermac:
	curl -o swaggerapp  -L https://github.com/go-swagger/go-swagger/releases/download/v0.22.0/swagger_darwin_amd64 && chmod +x swaggerapp

swaggerlin:
	curl -o swaggerapp  -L https://github.com/go-swagger/go-swagger/releases/download/v0.22.0/swagger_linux_amd64 && chmod +x swaggerapp

swaggerspec:
	 ./swaggerapp generate spec -m -o swagger.json

migrate/up:
	env ENV=local ./bin/migrate up

migrate/down:
	env ENV=local ./bin/migrate down

migrate/create:
	@read -p "Enter migration name: " name; \
	env ENV=local ./bin/migrate create $$name

code/domain:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --domain=true --name=$$name

code/core:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --core=true --name=$$name

code/psql:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --psql=true --name=$$name

code/controller:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --controller=true --name=$$name

code/route:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --route=true --name=$$name

code/crud:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --controller=true --route=true --psql=true --core=true --domain=true --name=$$name

code/tests:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --tests=true --name=$$name

format:
	@read -p "Enter filepath: " name; \
	gofmt -s -w ./$$name;

translate:
	env ENV=local ./bin/translator

lint:
	golangci-lint run --print-linter-name --enable-all --exclude dupl