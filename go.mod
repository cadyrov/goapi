module goapi

go 1.15

require (
	github.com/cadyrov/godict v1.0.7
	github.com/cadyrov/goerr v1.0.12
	github.com/cadyrov/gopsql v1.0.35
	github.com/cadyrov/govalidation v1.1.11
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.7.0
	github.com/golangci/golangci-lint v1.31.0 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.7.0
	github.com/mailgun/mailgun-go/v4 v4.1.3
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gopkg.in/yaml.v2 v2.3.0
	mvdan.cc/gofumpt v0.0.0-20200802201014-ab5a8192947d // indirect
)
