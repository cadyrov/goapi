package utils

import (
	"strings"

	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
)

func ToLike(query *string) (result string) {
	if query == nil {
		return
	}

	return "%" + strings.Join(strings.Split(strings.ToLower(*query), " "), "%") + "%"
}

type CounterErr struct {
	Err   goerr.IError
	Total int
}

func TotalFromQuery(channel chan CounterErr, builder gopsql.Builder, q gopsql.Queryer) {
	builder.Select("COUNT (*)")
	builder.Pagination(0, 0)

	ce := CounterErr{}

	res, e := q.QueryRow(builder.RawSQL(), builder.Values()...)
	if e != nil {
		ce.Err = e

		channel <- ce

		return
	}

	err := res.Scan(&ce.Total)
	if err != nil {
		ce.Err = e

		channel <- ce

		return
	}

	channel <- ce
}
