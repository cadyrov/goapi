package utils

import (
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func ServiceNameFromContext(r *http.Request, jwtname string) (serviceName string) {
	if client := r.Context().Value(jwtname); client != nil {
		serviceName = client.(string)
	}

	return serviceName
}

func StringSliceExist(slice []string, val string) bool {
	for i := range slice {
		if val == slice[i] {
			return true
		}
	}

	return false
}

func RootPath() (rootPath string) {
	rPath, err := filepath.Abs("")
	if err != nil {
		panic(err)
	}

	paths := strings.Split(rPath, "/")

	rootPath = findPath(paths, "Makefile")

	return rootPath
}

func findPath(paths []string, filename string) string {
	pth := strings.Join(paths, "/")

	if len(paths) == 0 {
		return ""
	}

	if _, err := os.Stat(pth + "/" + filename); os.IsNotExist(err) {
		return findPath(paths[:len(paths)-1], filename)
	}

	return pth
}
