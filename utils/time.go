package utils

import "time"

func TimePtr(in time.Time) *time.Time {
	return &in
}
