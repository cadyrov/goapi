package utils

import (
	"crypto/rand"
	"encoding/base64"
)

func StringPtr(in string) *string {
	return &in
}

func RString(length int) (string, error) {
	b := make([]byte, length)

	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), err
}
