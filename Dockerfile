FROM golang:1.15.3-alpine3.12 AS build
RUN apk add --no-cache git
ENV GO111MODULE=on
WORKDIR /go/src/apigo/
COPY ./ /go/src/apigo/
RUN go mod vendor
RUN apk add --no-cache make
RUN make build
RUN make migrate/up

FROM build AS swagger
RUN apk add --no-cache curl
RUN curl -o /swagger -L https://github.com/go-swagger/go-swagger/releases/download/v0.22.0/swagger_linux_amd64
RUN chmod +x /swagger
WORKDIR /go/src/apigo/
RUN /swagger generate spec -m -o /swagger.json


FROM alpine:latest AS api
EXPOSE 8080
COPY --from=build /go/src/apigo /apigo/
COPY --from=build /go/src/apigo/resources/config /apigo/resources/config
COPY --from=build /go/src/apigo/resources/i18n /apigo/resources/i18n
COPY --from=swagger /swagger.json /apigo/
WORKDIR /apigo/
ARG ENV=local
ENV ENV="${ENV}"
ENTRYPOINT ["/apigo/bin/apigo"]
