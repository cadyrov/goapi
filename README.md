# goapi

Blank project with authorisation and user func

instal dockerCe
``` 
sudo dpkg --configure -a
sudo apt update
sudo apt install docker.io       
sudo systemctl enable --now docker
apt install -y docker-compose
```
   

on project used linter 

```
golangci-lint run --print-linter-name --enable-all --exclude dupl
```

check config

```
create db settings, mail setting, jwt settings
```

check localisation

```
 check language
```

make commands
```
prune  - clean docker stack
	
image - create docker image
	
build - build and run app
	
swaggermac  - create library swagger for mac

swaggerlin - create library swagger for linux

swaggerspec - create swagger specification

migrate/up  - run migration

migrate/down  - down last migration
	
migrate/create - create new migration

code/domain -  codegen based on tablename

code/core -  codegen based on tablename

code/psql - codegen based on tablename

code/controller -  codegen based on tablename

code/route -  codegen based on tablename

translate - to update translate data yml
