package logger

import (
	"fmt"
	"io"
	"time"
)

type Color string

const (
	ColorBlack  Color = "\u001b[30m"
	ColorRed    Color = "\u001b[31m"
	ColorGreen  Color = "\u001b[32m"
	ColorYellow Color = "\u001b[33m"
	ColorBlue   Color = "\u001b[34m"
	ColorReset  Color = "\u001b[0m"
)

type Logger interface {
	Successf(in string, val ...interface{})
	Success(val ...interface{})
	Infof(in string, val ...interface{})
	Info(val ...interface{})
	Warnf(in string, val ...interface{})
	Warn(val ...interface{})
	Errf(in string, val ...interface{})
	Err(val ...interface{})
}

type osLogger struct {
	writer io.Writer
}

func New(writer io.Writer) Logger {
	return osLogger{writer: writer}
}

func (l osLogger) Infof(in string, val ...interface{}) {
	l.stdf(ColorReset, in, val...)
}

func (l osLogger) Info(val ...interface{}) {
	l.std(ColorReset, val...)
}

func (l osLogger) Errf(in string, val ...interface{}) {
	l.stdf(ColorRed, in, val...)
}

func (l osLogger) Err(val ...interface{}) {
	l.std(ColorRed, val...)
}

func (l osLogger) Warnf(in string, val ...interface{}) {
	l.stdf(ColorYellow, in, val...)
}

func (l osLogger) Warn(val ...interface{}) {
	l.std(ColorYellow, val...)
}

func (l osLogger) Successf(in string, val ...interface{}) {
	l.stdf(ColorGreen, in, val...)
}

func (l osLogger) Success(val ...interface{}) {
	l.std(ColorGreen, val...)
}

func (l osLogger) stdf(color Color, in string, val ...interface{}) {
	_, _ = fmt.Fprintf(l.writer, "%s ", color)
	_, _ = fmt.Fprintf(l.writer, "%s ", time.Now())
	_, _ = fmt.Fprintf(l.writer, "%s ", ColorReset)
	_, _ = fmt.Fprintf(l.writer, in, val...)
	_, _ = fmt.Fprint(l.writer, "\n")
}

func (l osLogger) std(color Color, val ...interface{}) {
	_, _ = fmt.Fprintf(l.writer, "%s ", color)
	_, _ = fmt.Fprintf(l.writer, "%s ", time.Now())
	_, _ = fmt.Fprintf(l.writer, "%s ", ColorReset)
	_, _ = fmt.Fprint(l.writer, val...)
	_, _ = fmt.Fprint(l.writer, "\n")
}
