package mailgun

import (
	"context"
	"time"

	"github.com/cadyrov/goerr"
	"github.com/mailgun/mailgun-go/v4"
)

type Config struct {
	PrivateAPIKey string        `yaml:"privateAPIKey"`
	Domain        string        `yaml:"domain"`
	Multiplier    time.Duration `yaml:"multiplier"`
}

type MailGun struct {
	MailGun *mailgun.MailgunImpl
	Config
}

func NewMailGunService(config Config) *MailGun {
	return &MailGun{MailGun: mailgun.NewMailgun(config.Domain, config.PrivateAPIKey), Config: config}
}

func (m *MailGun) NewMessage(sender, subject, body, recipientEmail string) *mailgun.Message {
	return m.MailGun.NewMessage(sender, subject, body, recipientEmail)
}

func (m *MailGun) Send(message *mailgun.Message) (
	response, id string, e goerr.IError) {
	messageContext, cancel := context.WithTimeout(context.Background(), m.Multiplier*time.Second)
	defer cancel()

	response, id, err := m.MailGun.Send(messageContext, message)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	return
}
