package config

import (
	"goapi/internal/repository"
	"goapi/tools/mailer"
	"goapi/tools/mailgun"
	"io/ioutil"
	"os"
	"time"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

type Config struct {
	Project       ProjectConfig     `yaml:"project"`
	Repository    repository.Config `yaml:"repository"`
	S3            S3Config          `yaml:"s3"`
	Generator     `yaml:"generator"`
	Jwt           `yaml:"jwt"`
	Web           `yaml:"web"`
	mailer.Config `yaml:"mailer"`
	Security      `yaml:"security"`
	SentryConfig  `yaml:"sentry"`
	Mailgun       mailgun.Config `yaml:"mailgun"`
}

type ProjectConfig struct {
	Name          string        `yaml:"name"`
	Debug         bool          `yaml:"debug"`
	MigratePath   string        `yaml:"migratePath"`
	SwaggerPath   string        `yaml:"swaggerPath"`
	I18nPath      string        `yaml:"i18nPath"`
	DefaultLocale godict.Locale `yaml:"defaultDictionary"`
}

type SentryConfig struct {
	DSN   string `json:"dsn"`
	Debug bool   `json:"debug"`
}

type Jwt struct {
	Services  []string `yaml:"services"`
	SecretKey string   `yaml:"secretKey"`
	ExpiredAt int      `yaml:"expiredAt"`
	KeyLength int      `json:"keyLength"`
}

type Web struct {
	Host          string        `yaml:"host"`
	Port          int           `yaml:"port"`
	ReadTimeout   int           `yaml:"readTimeout"`
	WriteTimeout  int           `yaml:"writeTimeout"`
	IdleTimeout   int           `yaml:"idleTimeout"`
	CookieExpired time.Duration `yaml:"cookieExpired"`
	Cors          []string      `yaml:"cors"`
	SSLSertPath   string        `yaml:"sslSertPath"`
	SSLKeyPath    string        `yaml:"sslKeyPath"`
}

type S3Config struct {
	Endpoint  string `yaml:"endpoint"`
	AccessKey string `yaml:"accessKey"`
	SecretKey string `yaml:"secretKey"`
	Bucket    string `yaml:"bucket"`
}

type Generator struct {
	DomainPath               string `yaml:"domainPath"`
	DomainTemplatePath       string `yaml:"domainTemplatePath"`
	CorePath                 string `yaml:"corePath"`
	CoreTemplatePath         string `yaml:"coreTemplatePath"`
	RoutePath                string `yaml:"routePath"`
	RouteTemplatePath        string `yaml:"routeTemplatePath"`
	ControllerPath           string `yaml:"controllerPath"`
	ControllerTemplatePath   string `yaml:"controllerTemplatePath"`
	PSQLPath                 string `yaml:"psqlPath"`
	PSQLTemplatePath         string `yaml:"psqlTemplatePath"`
	TestEntitiesPath         string `yaml:"testEntitiesPath"`
	TestEntitiesTemplatePath string `yaml:"testEntitiesTemplatePath"`
	CoreTestsPath            string `yaml:"coreTestsPath"`
	CoreTestsTemplatePath    string `yaml:"coreTestsTemplatePath"`
}

type Security struct {
	CookieLiveHours int `yaml:"cookieLiveHours"`
}

func New(path string) (c *Config, e goerr.IError) {
	c = &Config{}

	f, err := os.Open(path)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	data, err := ioutil.ReadAll(f)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	if e = godict.YamlUnmarshal(data, c); e != nil {
		return
	}

	return c, e
}
