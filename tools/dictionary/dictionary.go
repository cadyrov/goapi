package dictionary

import (
	"fmt"
	"goapi/tools/config"
	"goapi/tools/logger"
	"goapi/utils"
	"io/ioutil"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

type Dictionary struct {
	dictionaries  map[int]*godict.Dictionary
	defaultLocale godict.Locale
	config        *config.Config
	logger        logger.Logger
}

func New(defaultLocale godict.Locale, config *config.Config,
	logger logger.Logger) (d *Dictionary, e goerr.IError) {
	d = &Dictionary{
		dictionaries:  make(map[int]*godict.Dictionary),
		config:        config,
		logger:        logger,
		defaultLocale: defaultLocale,
	}

	e = d.prepareDictionaries()

	return d, e
}

func (d *Dictionary) ByLocale(locale godict.Locale) (dPtr *godict.Dictionary, e goerr.IError) {
	dPtr, ok := d.dictionaries[int(locale)]

	if !ok {
		d.logger.Info(fmt.Sprintf("%d wrong locale ", locale))

		dPtr, ok = d.dictionaries[int(d.defaultLocale)]

		if !ok {
			e = goerr.New(fmt.Sprintf("not found dictionaries %d %d", d.defaultLocale, locale))
		}
	}

	return dPtr, e
}

func (d *Dictionary) prepareDictionaries() (e goerr.IError) {
	files, err := ioutil.ReadDir(utils.RootPath() + "/" + d.config.Project.I18nPath)
	if err != nil {
		e = goerr.New("prepare i18n" + err.Error())

		return
	}

	for _, file := range files {
		if file.IsDir() {
			locale, err := strconv.Atoi(file.Name())
			if err != nil {
				e = goerr.New("prepare dictionary" + err.Error())

				return
			}

			vocabular := godict.Dictionary{}

			bt, err := ioutil.ReadFile(utils.RootPath() +
				"/" + d.config.Project.I18nPath + "/" + file.Name() + "/dictionary.yml")
			if err != nil {
				e = goerr.New("prepare dictionary" + err.Error())

				return
			}

			if e = godict.YamlUnmarshal(bt, &vocabular); e != nil {
				return
			}

			d.dictionaries[locale] = &vocabular
		}
	}

	d.logger.Info("dictionary prepared")

	return e
}
