package i18n

import (
	"fmt"
	"goapi/tools/config"
	"goapi/tools/logger"
	"goapi/utils"
	"io/ioutil"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"gopkg.in/yaml.v2"
)

type Translator interface {
	Translatef(godict.Locale, string, ...interface{}) string
}

func New(defaultLocale godict.Locale, config *config.Config,
	logger logger.Logger) (t Translator, e goerr.IError) {
	mt := &MemoryTranslator{
		defaultLocale: defaultLocale,
		logger:        logger,
		localeMap:     make(map[int]map[string]string),
	}

	e = mt.prepareI18n(config)

	return mt, e
}

type MemoryTranslator struct {
	defaultLocale godict.Locale
	localeMap     map[int]map[string]string
	logger        logger.Logger
}

func (mt *MemoryTranslator) prepareI18n(config *config.Config) (e goerr.IError) {
	files, err := ioutil.ReadDir(utils.RootPath() + "/" + config.Project.I18nPath)
	if err != nil {
		e = goerr.New("prepare i18n" + err.Error())

		return
	}

	for _, file := range files {
		if file.IsDir() {
			locale, err := strconv.Atoi(file.Name())
			if err != nil {
				e = goerr.New("prepare i18n" + err.Error())

				return
			}

			vocabulary := make(map[string]string)

			bt, err := ioutil.ReadFile(utils.RootPath() + "/" + config.Project.I18nPath + "/" + file.Name() + "/data.yml")
			if err != nil {
				e = goerr.New("prepare i18n" + err.Error())

				return
			}

			err = yaml.Unmarshal(bt, &vocabulary)
			if err != nil {
				e = goerr.New("prepare i18n" + err.Error())

				return
			}

			mt.localeMap[locale] = vocabulary
		}
	}

	mt.logger.Info("i18n prepared")

	return e
}

func (mt *MemoryTranslator) translate(locale godict.Locale, value string) string {
	if locale == 0 {
		locale = mt.defaultLocale
	}

	mp, ok := mt.localeMap[int(locale)]
	if !ok {
		mt.logger.Info(fmt.Sprintf("%d vocabulary not found", locale))

		return value
	}

	result, ok := mp[value]
	if !ok {
		mt.logger.Info(fmt.Sprintf("%s %d word not found on locale", value, locale))

		return value
	}

	return result
}

func (mt *MemoryTranslator) Translatef(locale godict.Locale, key string, val ...interface{}) string {
	return fmt.Sprintf(mt.translate(locale, key), val...)
}

func (mt *MemoryTranslator) TranslateErr(locale godict.Locale, err goerr.IError) goerr.IError {
	arrErr := err.GetDetails()

	for i := range arrErr {
		arrErr = append(arrErr, mt.TranslateErr(locale, arrErr[i]))
	}

	e := goerr.New(mt.translate(locale, err.GetMessage())).HTTP(err.GetCode()).SetID(err.GetID())

	for i := range arrErr {
		e.PushDetail(arrErr[i])
	}

	return e
}
