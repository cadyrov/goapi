BRANCH_NAME=$1

if [ $# -eq 0 ]; then
  echo "no branch name"
else
  cd /var/goapi.com
  git pull origin $BRANCH_NAME
  docker-compose up -d --build apigo
fi
